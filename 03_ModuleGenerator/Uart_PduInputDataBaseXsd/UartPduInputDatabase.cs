﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OsarResources.XML;
using OsarResources.Generic;
using System.Xml.Serialization;

namespace Uart_PduInputDataBaseXsd
{
  public struct UartSubDataSet
  {
    public UInt16 dataStartIndex;
    public UInt16 dataLength;
    public String dataName;
    public String dataDescription;
  }

  public struct Uart_PduCfg
  {
    public UInt16 pduId;                        /*!< ID of the PDU */
    public UInt16 dataLength;                   /*!< Length of used data within the PDU >> Without frame end pattern */
    public String pduShortName;                 /*!< Sort name of the PDU */
    public String pduDescription;               /*!< Unused Pdu Description */
    public List<UartSubDataSet> pduDataSet;     /*!< List with internal structure of the PDU */
  }

  public class UartPduInputDatabase
    {
    public XmlFileVersion xmlFileVersion;
    public List<Uart_PduCfg> pduConfigList;
  }
}
