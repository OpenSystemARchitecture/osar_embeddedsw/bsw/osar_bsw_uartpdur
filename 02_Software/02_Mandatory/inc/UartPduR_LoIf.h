/*****************************************************************************************************************************
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        25.03.2018 12:37:53                                                                                          *
 * @brief       Declaration of UartPduR Module Mendatroy Low Level Interfaces                                                *
 * @details     GlobalUartPduR Low Level Interfaces                                                                          *
 * @version     0.1.0                                                                                                        *
 * @libNote     OSAR Source File Library v.0.1.2.15                                                                          *
*****************************************************************************************************************************/
#ifndef __UARTPDUR_LOIF_H
#define __UARTPDUR_LOIF_H
/**
 * @addtogroup UartPduR
 * @{
 */

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 Start of include area                        << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#include "Std_Types.h"
#include "UartPduR_Types.h"
#include "UartIf_Types.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                  End of include area                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of public data types                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of public data types                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            Start of public definition area                   << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of public definition area                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of global variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of global variables                      << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>           Start of public function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/**
* @brief           API function to read a received data frame
* @param[in/out]   UartIf_RxFrameType   Higher layer frame data
* @retval          UartReturnType
*                  >> UARTIF_E_NULL_POINTER
*                  >> UARTIF_E_BUFFER_EMPTY
*                  >> UARTIF_E_BUFFER_OVERFLOW
*                  >> UARTIF_E_OK
*                  >> UARTIF_E_NOT_OK
* @details         This function could be used to read a receive data frame from an uart module
*/
extern UartIf_ReturnType UartIf_GetReceivedData(UartIf_RxFrameType *dataFrame);

/**
* @brief           API function to set a transmit data frame
* @param[in/out]   UartIf_TxFrameType   Higher layer frame data
* @retval          UartReturnType
*                  >> UARTIF_E_NULL_POINTER
*                  >> UARTIF_E_BUFFER_EMPTY
*                  >> UARTIF_E_BUFFER_OVERFLOW
*                  >> UARTIF_E_OK
*                  >> UARTIF_E_NOT_OK
* @details         This function could be used to set a new transmit data frame for the uart module
*/
extern UartIf_ReturnType UartIf_SetTransmitData(UartIf_TxFrameType *dataFrame);
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of public function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/**
 * @}
 */

#endif /* __UARTPDUR_LOIF_H*/
