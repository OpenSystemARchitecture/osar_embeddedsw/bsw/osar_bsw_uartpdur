/*****************************************************************************************************************************
* @file        main.cpp                                                                                                     *
* @author      OSAR Team                                                                                                    *
* @date        20.02.2018 10:49:55                                                                                          *
* @brief       Implementation of unit test functionalities from the "Dummy" module.                                         *
*****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 Start of include area                        << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <vcruntime.h>
#include <cmocka.h>
#include <stdio.h>
#include "stubs.h"
#include "UartPduR.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                  End of include area                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of private data types                   << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of private data types                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            Start of private definitions area                 << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of private definitions area                  << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/



/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>           Start of private function prototypes               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/* Unit tests of Group SendAPdu */
static void UnitTest_UartPduR_API_SendPdu_001(void **state);
static void UnitTest_UartPduR_API_SendPdu_002(void **state);
static void UnitTest_UartPduR_API_SendPdu_003(void **state);
static void UnitTest_UartPduR_API_SendPdu_004(void **state);
static void UnitTest_UartPduR_API_ReceivePdu_001(void **state);
static void UnitTest_UartPduR_API_ReceivePdu_002(void **state);
static void UnitTest_UartPduR_API_ReceivePdu_003(void **state);
/* Unit tests of Group x */
/*...*/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            End of private function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of global variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/* Creating Unit Test Group ... */
const struct CMUnitTest tests[] = {
  cmocka_unit_test(UnitTest_UartPduR_API_SendPdu_001),
  cmocka_unit_test(UnitTest_UartPduR_API_SendPdu_002),
  cmocka_unit_test(UnitTest_UartPduR_API_SendPdu_003),
  cmocka_unit_test(UnitTest_UartPduR_API_SendPdu_004),
  cmocka_unit_test(UnitTest_UartPduR_API_ReceivePdu_001),
  cmocka_unit_test(UnitTest_UartPduR_API_ReceivePdu_002),
  cmocka_unit_test(UnitTest_UartPduR_API_ReceivePdu_003),
};
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of global variables                      << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
extern uint8 testData[100];
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of private variables                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of private variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>        Start of private function implementation              << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/*--------------------------------------------------------------------------------------------------------------------------*/
/*------------------------------------------------ Unit Tests of group ... -------------------------------------------------*/
/*--------------------------------------------------------------------------------------------------------------------------*/
static void UnitTest_UartPduR_API_SendPdu_001(void **state)
{
  UartIf_ReturnType retVal;
  /*----------------------------------------------------------------------------*/
  /*------------------------------- Test Setup ---------------------------------*/
  /*----------------------------------------------------------------------------*/
  printf("[ DESCRIP  ] >> Send a valid PDU with a valid Module id and a low byte Id<< \r\n");
  printf("             >>\r\n");

  expect_function_call(UartIf_SetTransmitData);
  will_return(UartIf_SetTransmitData, 10);
  will_return(UartIf_SetTransmitData, 0);
  will_return(UartIf_SetTransmitData, UARTIF_E_OK);
  testData[0] = 0; testData[1] = 0;
  testData[2] = 1;
  testData[3] = 2;
  testData[4] = 3;
  testData[5] = 4;
  testData[6] = 5;
  testData[7] = 6;
  testData[8] = 0x0D;  testData[9] = 0x0A;


  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Execution ------------------------------*/
  /*----------------------------------------------------------------------------*/

  retVal = UartPduR_SendUartPdu(0x0, &testData[2], 6);
  assert_int_equal(retVal, UARTPDUR_E_OK);

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Cleanup --------------------------------*/
  /*----------------------------------------------------------------------------*/
  (void)state; /* unused */
}

static void UnitTest_UartPduR_API_SendPdu_002(void **state)
{
  UartIf_ReturnType retVal;
  /*----------------------------------------------------------------------------*/
  /*------------------------------- Test Setup ---------------------------------*/
  /*----------------------------------------------------------------------------*/
  printf("[ DESCRIP  ] >> Send a valid PDU with a valid Module id and a two byte Id<< \r\n");
  printf("             >>\r\n");

  expect_function_call(UartIf_SetTransmitData);
  will_return(UartIf_SetTransmitData, 10);
  will_return(UartIf_SetTransmitData, 0);
  will_return(UartIf_SetTransmitData, UARTIF_E_OK);
  testData[0] = 5; testData[1] = 10;
  testData[2] = 10;
  testData[3] = 20;
  testData[4] = 30;
  testData[5] = 40;
  testData[6] = 50;
  testData[7] = 60;
  testData[8] = 0x0D;  testData[9] = 0x0A;

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Execution ------------------------------*/
  /*----------------------------------------------------------------------------*/

  retVal = UartPduR_SendUartPdu(0x050A, &testData[2], 6);
  assert_int_equal(retVal, UARTPDUR_E_OK);

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Cleanup --------------------------------*/
  /*----------------------------------------------------------------------------*/
  (void)state; /* unused */
}

static void UnitTest_UartPduR_API_SendPdu_003(void **state)
{
  UartIf_ReturnType retVal;
  /*----------------------------------------------------------------------------*/
  /*------------------------------- Test Setup ---------------------------------*/
  /*----------------------------------------------------------------------------*/
  printf("[ DESCRIP  ] >> Send a valid PDU with a valid Module id and an unknown pdu id << \r\n");
  printf("             >>\r\n");

  will_return(Det_ReportError, UARTPDUR_DET_MODULE_ID);
  will_return(Det_ReportError, UARTPDUR_E_ID_UNKNOWN);
  will_return(Det_ReportError, E_OK);

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Execution ------------------------------*/
  /*----------------------------------------------------------------------------*/

  retVal = UartPduR_SendUartPdu(100, &testData[2], 6);
  assert_int_equal(retVal, UARTPDUR_E_ID_UNKNOWN);

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Cleanup --------------------------------*/
  /*----------------------------------------------------------------------------*/
  (void)state; /* unused */
}

static void UnitTest_UartPduR_API_SendPdu_004(void **state)
{
  UartIf_ReturnType retVal;
  /*----------------------------------------------------------------------------*/
  /*------------------------------- Test Setup ---------------------------------*/
  /*----------------------------------------------------------------------------*/
  printf("[ DESCRIP  ] >> Send a valid PDU but transmitt buffer is full << \r\n");
  printf("             >>\r\n");

  expect_function_call(UartIf_SetTransmitData);
  will_return(UartIf_SetTransmitData, 10);
  will_return(UartIf_SetTransmitData, 0);
  will_return(UartIf_SetTransmitData, UARTIF_E_BUFFER_OVERFLOW);
  testData[0] = 0; testData[1] = 0;
  testData[2] = 1;
  testData[3] = 2;
  testData[4] = 3;
  testData[5] = 4;
  testData[6] = 5;
  testData[7] = 6;
  testData[8] = 0x0D;  testData[9] = 0x0A;

  will_return(Det_ReportError, UARTPDUR_DET_MODULE_ID);
  will_return(Det_ReportError, UARTPDUR_E_TRANSMITBUFFER_OVERFLOW);
  will_return(Det_ReportError, E_OK);

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Execution ------------------------------*/
  /*----------------------------------------------------------------------------*/

  retVal = UartPduR_SendUartPdu(0x0, &testData[2], 6);
  assert_int_equal(retVal, UARTPDUR_E_TRANSMITBUFFER_OVERFLOW);

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Cleanup --------------------------------*/
  /*----------------------------------------------------------------------------*/
  (void)state; /* unused */
}

static void UnitTest_UartPduR_API_ReceivePdu_001(void **state)
{
  UartIf_ReturnType retVal;
  /*----------------------------------------------------------------------------*/
  /*------------------------------- Test Setup ---------------------------------*/
  /*----------------------------------------------------------------------------*/
  printf("[ DESCRIP  ] >> Receive a valid PDU << \r\n");
  printf("             >>\r\n");

  expect_function_call(UartIf_GetReceivedData);
  will_return(UartIf_GetReceivedData, 10);
  will_return(UartIf_GetReceivedData, 0);
  will_return(UartIf_GetReceivedData, UARTIF_E_OK);
  testData[0] = 0; testData[1] = 0;
  testData[2] = 1;
  testData[3] = 2;
  testData[4] = 3;
  testData[5] = 4;
  testData[6] = 5;
  testData[7] = 6;
  testData[8] = 0x0D;  testData[9] = 0x0A;

  expect_function_call(testReceiveFncPduId0);
  will_return(testReceiveFncPduId0, 6);

  expect_function_call(UartIf_GetReceivedData);
  will_return(UartIf_GetReceivedData, 10);
  will_return(UartIf_GetReceivedData, 0);
  will_return(UartIf_GetReceivedData, UARTIF_E_BUFFER_EMPTY);
  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Execution ------------------------------*/
  /*----------------------------------------------------------------------------*/

  UartPduR_Mainfunction();

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Cleanup --------------------------------*/
  /*----------------------------------------------------------------------------*/
  (void)state; /* unused */
}


static void UnitTest_UartPduR_API_ReceivePdu_002(void **state)
{
  UartIf_ReturnType retVal;
  /*----------------------------------------------------------------------------*/
  /*------------------------------- Test Setup ---------------------------------*/
  /*----------------------------------------------------------------------------*/
  printf("[ DESCRIP  ] >> Receive a invalid PDU >> Unknown PDU ID<< \r\n");
  printf("             >>\r\n");

  expect_function_call(UartIf_GetReceivedData);
  will_return(UartIf_GetReceivedData, 10);
  will_return(UartIf_GetReceivedData, 0);
  will_return(UartIf_GetReceivedData, UARTIF_E_OK);
  testData[0] = 0; testData[1] = 100;
  testData[2] = 1;
  testData[3] = 2;
  testData[4] = 3;
  testData[5] = 4;
  testData[6] = 5;
  testData[7] = 6;
  testData[8] = 0x0D;  testData[9] = 0x0A;

  expect_function_call(UartIf_GetReceivedData);
  will_return(UartIf_GetReceivedData, 10);
  will_return(UartIf_GetReceivedData, 0);
  will_return(UartIf_GetReceivedData, UARTIF_E_BUFFER_EMPTY);
  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Execution ------------------------------*/
  /*----------------------------------------------------------------------------*/

  UartPduR_Mainfunction();

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Cleanup --------------------------------*/
  /*----------------------------------------------------------------------------*/
  (void)state; /* unused */
}

static void UnitTest_UartPduR_API_ReceivePdu_003(void **state)
{
  UartIf_ReturnType retVal;
  /*----------------------------------------------------------------------------*/
  /*------------------------------- Test Setup ---------------------------------*/
  /*----------------------------------------------------------------------------*/
  printf("[ DESCRIP  ] >> Receive a valid PDU but without a notification function << \r\n");
  printf("             >>\r\n");

  expect_function_call(UartIf_GetReceivedData);
  will_return(UartIf_GetReceivedData, 10);
  will_return(UartIf_GetReceivedData, 0);
  will_return(UartIf_GetReceivedData, UARTIF_E_OK);
  testData[0] = 0; testData[1] = 10;
  testData[2] = 1;
  testData[3] = 2;
  testData[4] = 3;
  testData[5] = 4;
  testData[6] = 5;
  testData[7] = 6;
  testData[8] = 0x0D;  testData[9] = 0x0A;

  expect_function_call(UartIf_GetReceivedData);
  will_return(UartIf_GetReceivedData, 10);
  will_return(UartIf_GetReceivedData, 0);
  will_return(UartIf_GetReceivedData, UARTIF_E_BUFFER_EMPTY);
  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Execution ------------------------------*/
  /*----------------------------------------------------------------------------*/

  UartPduR_Mainfunction();

  /*----------------------------------------------------------------------------*/
  /*------------------------------ Test Cleanup --------------------------------*/
  /*----------------------------------------------------------------------------*/
  (void)state; /* unused */
}

/*--------------------------------------------------------------------------------------------------------------------------*/
/*------------------------------------------------ Unit Tests of group ... -------------------------------------------------*/
/*--------------------------------------------------------------------------------------------------------------------------*/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>         End of private function implementation               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>          Start of public function implementation             << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#define CNT_OF_TEST_GROUPS 1
int main()
{
  int result[CNT_OF_TEST_GROUPS], idx = 0;
  /* Setup Console for Test Output */
  printf("Startup of Module Tests for Module xyz. \r\nTest Framework: CMocka 1.1.1 \r\n");

  /*#################################### Run CMocka group tests #############################################*/
  result[0] = cmocka_run_group_tests(tests, NULL, NULL);

  /* Print result */
  printf("\r\n\r\n=====================================================================================\r\n");
  printf("Test summary:\r\n\r\n");
  for (idx = 0; idx < CNT_OF_TEST_GROUPS; idx++)
  {
    printf("Testgroup %d >> Cnt of errors: %d\r\n", idx, result[idx]);
  }
  printf("=====================================================================================\r\n\r\n");

  /* wait for user key to shutdown system */
  printf("Pres any key to exit test environment \r\n");
  
  getch();
    return 0;
}
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>          End of public function implementation               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

