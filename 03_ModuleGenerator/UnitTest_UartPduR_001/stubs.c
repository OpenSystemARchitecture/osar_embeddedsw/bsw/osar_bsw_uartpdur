/*****************************************************************************************************************************
* @file        Stubs.c                                                                                                      *
* @author      OSAR Team                                                                                                    *
* @date        20.02.2018 10:49:55                                                                                          *
* @brief       Implementation of functionalities from the "Stubs" module.                                                   *
*****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 Start of include area                        << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#include "stubs.h"
#include <vcruntime.h>
#include "cmocka.h"
#include "UartIf_Types.h"
#include "Std_Types.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                  End of include area                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of private data types                   << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of private data types                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            Start of private definitions area                 << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of private definitions area                  << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of global variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
uint8 testData[100];
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of global variables                      << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of private variables                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of private variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>           Start of private function prototypes               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            End of private function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>        Start of private function implementation              << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>         End of private function implementation               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>          Start of public function implementation             << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
Std_ReturnType Det_ReportError(uint8 moduleId, uint8 errorId)
{
  assert_int_equal(moduleId, mock());
  assert_int_equal(errorId, mock());
  return (Std_ReturnType)mock();
}

/**
* @brief           API function to read a received data frame
* @param[in/out]   UartIf_RxFrameType   Higher layer frame data
* @retval          UartReturnType
*                  >> UARTIF_E_NULL_POINTER
*                  >> UARTIF_E_BUFFER_EMPTY
*                  >> UARTIF_E_BUFFER_OVERFLOW
*                  >> UARTIF_E_OK
*                  >> UARTIF_E_NOT_OK
* @details         This function could be used to read a receive data frame from an uart module
*/
UartIf_ReturnType UartIf_GetReceivedData(UartIf_RxFrameType *dataFrame)
{
  function_called();
  uint8 dataLength = mock(), idx = 0;
  uint8 moduleId = mock();
  
  dataFrame->cntOfData = dataLength;
  dataFrame->moduleID = moduleId;

  for (idx = 0; idx < dataLength; idx++)
  {
    dataFrame->pData[idx] = testData[idx];
  }
  
  return mock();
}

/**
* @brief           API function to set a transmit data frame
* @param[in/out]   UartIf_TxFrameType   Higher layer frame data
* @retval          UartReturnType
*                  >> UARTIF_E_NULL_POINTER
*                  >> UARTIF_E_BUFFER_EMPTY
*                  >> UARTIF_E_BUFFER_OVERFLOW
*                  >> UARTIF_E_OK
*                  >> UARTIF_E_NOT_OK
* @details         This function could be used to set a new transmit data frame for the uart module
*/
UartIf_ReturnType UartIf_SetTransmitData(UartIf_TxFrameType *dataFrame)
{
  uint8 idx = 0;
  function_called();

  assert_int_equal(dataFrame->cntOfData, mock());
  assert_int_equal(dataFrame->moduleID, mock());
  
  for (idx = 0; idx < dataFrame->cntOfData; idx++)
  {
    assert_int_equal(dataFrame->pData[idx], testData[idx]);
  }

  return mock();
}

void testReceiveFncPduId0(uint8* data, uint16 length)
{
  int idx;
  function_called();
  assert_int_equal(length, mock());

  for (idx = 0; idx < length; idx++)
  {
    assert_int_equal(data[idx], testData[idx + 2]);
  }
}

void testReceiveFncPduId1(uint8* data, uint16 length)
{
  function_called();
  assert_int_equal(data, mock());
  assert_int_equal(length, mock());
}

void testReceiveFncPduId4(uint8* data, uint16 length)
{
  function_called();
  assert_int_equal(data, mock());
  assert_int_equal(length, mock());
}
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>          End of public function implementation               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/**
* @}
*/

