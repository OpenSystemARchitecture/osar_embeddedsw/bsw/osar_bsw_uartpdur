﻿/*****************************************************************************************************************************
 * @file        WorkingDataBaseM.cs                                                                                          *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        26.03.2019                                                                                                   *
 * @brief       Implementation of the Working Data Base Data Model                                                           *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
 * @addtogroup ModuleLibrary.Versions.v_1_0_3.Models
 * @{
 */

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using OsarResources.XML;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace ModuleLibrary.Versions.v_1_0_3.Models
{
  public class WorkingDataBaseViewModel
  {
    UartPduR_PduCfg pduConfigElement; 
    public WorkingDataBaseViewModel(UartPduR_PduCfg cfgElement)
    {
      pduConfigElement = cfgElement;
    }

    public UartPduR_PduCfg PduConfigElement
    {
      get => pduConfigElement; set => pduConfigElement = value;
    }

    public byte ModuleId
    { get => pduConfigElement.moduleID; set => pduConfigElement.moduleID = value; }

    public UInt16 PduId
    { get => pduConfigElement.pduId; set => pduConfigElement.pduId = value; }

    public UInt16 DataLength
    { get => pduConfigElement.dataLength; set => pduConfigElement.dataLength = value; }

    public String RxNotificationFnc
    { get => pduConfigElement.rxNotificationFnc; set => pduConfigElement.rxNotificationFnc = value; }

    public String PduShortName
    { get => pduConfigElement.pduShortName; set => pduConfigElement.pduShortName = value; }

    public String PduDescription
    { get => pduConfigElement.pduDescription; set => pduConfigElement.pduDescription = value; }
  }

}
