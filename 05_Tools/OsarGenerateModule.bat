
set "basePath=%~dp0.\..\02_Software\"
set "cfgFilePath=.\01_Generator\UartPduRModuleCfg.xml"
set "libFilePath=.\01_Generator\UartPduRModuleLibrary.dll"

set cfgFilePathStr=%basePath%%cfgFilePath%
set libFilePathStr=%basePath%%libFilePath%

OsarStandaloneModuleGenerator.exe -m %basePath% -c %cfgFilePathStr% -l %libFilePathStr% -g