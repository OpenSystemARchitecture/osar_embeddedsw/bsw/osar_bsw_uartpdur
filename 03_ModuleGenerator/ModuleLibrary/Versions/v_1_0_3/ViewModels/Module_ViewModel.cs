﻿/*****************************************************************************************************************************
 * @file        Module_ViewModel.cs                                                                                          *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        26.03.2019                                                                                                   *
 * @brief       Implementation of the View Model from the Module Xml Cfg Class                                               *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
 * @addtogroup ModuleLibrary.Versions.v_1_0_3.ViewModels
 * @{
 */

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OsarResources.XML;
using OsarResources.Generic;
using System.Xml.Serialization;
using System.IO;
using OsarResources.Generator;
using PropertyChanged;


using System.Collections.ObjectModel;
using System.ComponentModel;
using ModuleLibrary.Versions.v_1_0_3.Models;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace ModuleLibrary.Versions.v_1_0_3.ViewModels
{
  /// <summary>
  /// Delegate to store the Configuration
  /// </summary>
  public delegate void StoreConfiguration();

  public class Module_ViewModel : INotifyPropertyChanged, OsarModuleGeneratorInterface
  {
    private static Models.UartPduRXml UartPduRXmlCfg = new Models.UartPduRXml();
    private static string pathToConfiguratioFile;
    private static string pathToModuleBaseFolder;
    //private static StoreConfiguration storeCfgModule = new StoreConfiguration(SaveModuleConfigToXml);
    private static WorkingDataBaseVM workingDataBaseVm;
    private static ObservableCollection<string> inputDataBasesFiles;
    private static ObservableCollection<WorkingDataBaseViewModel> workingDataBaseData;

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="pathToCfgFile"> Should be a absolute Path</param>
    /// <param name="absPathToBaseModuleFolder"> Should be a absolute Path</param>
    public Module_ViewModel(string pathToCfgFile, string absPathToBaseModuleFolder)
    {
      /* Check if config file path is an rooted one */
      if(true == Path.IsPathRooted(pathToCfgFile))
      {
        pathToConfiguratioFile = pathToCfgFile;
      }
      else
      { 
        pathToConfiguratioFile = absPathToBaseModuleFolder + pathToCfgFile; 
      }

      pathToModuleBaseFolder = absPathToBaseModuleFolder;

      /* Read Configuration File */
      if (!File.Exists(pathToConfiguratioFile))
      {
        // If config file does not exist, create a default file
        SetDefaultConfiguration();
      }

      /* Read Configuration File */
      ReadConfigFromXml();

      /* Create Working Data Base View Model */
      workingDataBaseVm = new WorkingDataBaseVM(Path.GetDirectoryName(pathToConfiguratioFile) + UartPduRXmlCfg.pathOfWorkingDataBase);

      /* Initialize Input Data Base File List */
      inputDataBasesFiles = new ObservableCollection<string>();
      for (int idx = 0; idx < UartPduRXmlCfg.pathOfInputDataBases.Count; idx++)
      {
        inputDataBasesFiles.Add(UartPduRXmlCfg.pathOfInputDataBases[idx]);
      }

      /* Initialize the working data base data list */
      workingDataBaseData = new ObservableCollection<WorkingDataBaseViewModel>();
      foreach (UartPduR_PduCfg element in workingDataBaseVm.UartPduRWorkingdataBaseXmlFile.pduConfigList)
      {
        workingDataBaseData.Add(new WorkingDataBaseViewModel(element));
      }

    }

    #region INotifyPropertyChanged Interface implementation
    /// <summary>
    /// The event that is fired when any child property changes its value
    /// </summary>
    public event PropertyChangedEventHandler PropertyChanged = (sender, e) =>
    {
      SaveModuleConfigToXml();
    };

    /// <summary>
    /// Encapsulation of property changed event
    /// </summary>
    /// <param name="e"></param>
    protected virtual void OnPropertyChanged(PropertyChangedEventArgs e)
    {
      if (null != PropertyChanged)
      {
        PropertyChanged(this, e);
      }
    }
    #endregion

    #region Module Configuration File Attributes
    /// <summary>
    /// Interface for the XML File Version
    /// </summary>
    public XmlFileVersion XmlFileVersion
    {
      get { return UartPduRXmlCfg.xmlFileVersion; }
      set { UartPduRXmlCfg.xmlFileVersion = value; }
    }

    /// <summary>
    /// Interface for the XML File Version string
    /// </summary>
    public string XmlFileVersion_String
    {
      get
      {
        return ( "v." + UartPduRXmlCfg.xmlFileVersion.MajorVersion.ToString() + "." +
          UartPduRXmlCfg.xmlFileVersion.MinorVersion.ToString() + "." +
          UartPduRXmlCfg.xmlFileVersion.PatchVersion.ToString() );
      }
    }

    /// <summary>
    /// Interface for the Det Module Id
    /// </summary>
    public UInt16 DetModuleID
    {
      get { return UartPduRXmlCfg.detModuleID; }
      set { UartPduRXmlCfg.detModuleID = value; }
    }

    /// <summary>
    /// Interface for the Det Module Usage
    /// </summary>
    public SystemState DetModuleUsage
    {
      get { return UartPduRXmlCfg.detModuleUsage; }
      set { UartPduRXmlCfg.detModuleUsage = value; }
    }

    /// <summary>
    /// Interface for the Module Version
    /// </summary>
    public string ModuleVersion
    {
      get { return Generator.DefResources.ModuleVersion; }
    }

    /// <summary>
    /// Interface for the Module Mainfunction cycle time
    /// </summary>
    public UInt16 MainfunctionCycleTime
    {
      get => UartPduRXmlCfg.miBMainfunctionCycleTimeMs;
      set => UartPduRXmlCfg.miBMainfunctionCycleTimeMs = value;
    }

    /// <summary>
    /// Interface to the CRC32 E2E communication
    /// </summary>
    public SystemState Crc32E2EUsage
    {
      get => UartPduRXmlCfg.useCrc32EthernetE2ECommunication;
      set => UartPduRXmlCfg.useCrc32EthernetE2ECommunication = value;
    }

    /// <summary>
    /// Interface for the max frame data size
    /// </summary>
    public UInt16 UartIfMayFrameDataSize
    {
      get => UartPduRXmlCfg.uartIfMaxFrameDataSize;
      set => UartPduRXmlCfg.uartIfMaxFrameDataSize = value;
    }

    /// <summary>
    /// Interface for the working data base path
    /// </summary>
    public string WorkingDataBaseFilePath
    {
      get => UartPduRXmlCfg.pathOfWorkingDataBase;
      set => UartPduRXmlCfg.pathOfWorkingDataBase = value;
    }

    /// <summary>
    /// Interface for the uart frame end pattern
    /// </summary>
    public string UartFrameEndPattern
    {
      get
      {
        List<string> temp = new List<string>();

        foreach(byte element in UartPduRXmlCfg.uartFrameEndPatternElements)
        {
          temp.Add(element.ToString("X2"));
        }

        return String.Join("", temp.ToArray());
      }
      set
      {
        UartPduRXmlCfg.uartFrameEndPatternElements =
          Enumerable.Range(0, value.Length)
                    .Where(x => x % 2 == 0)
                    .Select(x => Convert.ToByte(value.Substring(x, 2), 16))
                    .ToArray();
      }
    }

    /// <summary>
    /// Interface for the input data base files
    /// </summary>
    public ObservableCollection<string> InputDataBaseFiles
    {
      get => inputDataBasesFiles;
      set => inputDataBasesFiles = value;
    }

    /// <summary>
    /// Interface fo the input data base files
    /// </summary>
    public ObservableCollection<WorkingDataBaseViewModel> WorkingDataBaseUiData
    {
      get => workingDataBaseData;
      set => workingDataBaseData = value;
    }

    #endregion

    #region UI Actions
    /// <summary>
    /// Interface to add a new base module
    /// </summary>
    public void AddNewInputDataBaseFile(string filePath)
    {
      inputDataBasesFiles.Add(filePath);
      OnPropertyChanged(new PropertyChangedEventArgs(nameof(InputDataBaseFiles)));
    }

    /// <summary>
    /// Interface to remove a specific base module
    /// </summary>
    /// <param name="baseModule"></param>
    public void RemoveInputDataBaseFile(string filePath)
    {
      inputDataBasesFiles.Remove(filePath);
      OnPropertyChanged(new PropertyChangedEventArgs(nameof(InputDataBaseFiles)));
    }

    /// <summary>
    /// Interface to notify for an data base update
    /// </summary>
    public void WorkingDataBaseUpdated()
    {
      if (null != workingDataBaseData)
      {
        workingDataBaseVm.UartPduRWorkingdataBaseXmlFile.pduConfigList.Clear();
        foreach (var element in workingDataBaseData)
        {
          workingDataBaseVm.UartPduRWorkingdataBaseXmlFile.pduConfigList.Add(element.PduConfigElement);
        }
      }

      workingDataBaseVm.OnPropertyChanged(new PropertyChangedEventArgs(nameof(UartPduR_PduCfg)));
    }
    #endregion

    #region Configuration Load and Storage
    /*
     * @brief       Function to store the current configuration structure to an xml file
     * @param       none
     * @retval      none
     */
    protected static void SaveModuleConfigToXml()
    {
      // Write back UI page configuration data to config structure
      if (null != inputDataBasesFiles)
      {
        UartPduRXmlCfg.pathOfInputDataBases.Clear();
        foreach (var element in inputDataBasesFiles)
        {
          UartPduRXmlCfg.pathOfInputDataBases.Add(element);
        }
      }

      XmlSerializer writer = new XmlSerializer(UartPduRXmlCfg.GetType());
      StreamWriter file = new StreamWriter(pathToConfiguratioFile);
      writer.Serialize(file, UartPduRXmlCfg);
      file.Close();
    }

    /*
     * @brief       Function to read the current configuration structure from an xml file
     * @param       none
     * @retval      none
     */
    private static void ReadConfigFromXml()
    {
      XmlSerializer reader = new XmlSerializer(UartPduRXmlCfg.GetType());
      StreamReader file = new StreamReader(pathToConfiguratioFile);
      UartPduRXmlCfg = ( Models.UartPduRXml)reader.Deserialize(file);
      file.Close();
    }
    #endregion

    #region Generic Osar Generator
    /// <summary>
    /// Used to validate the active configuration of its correctness.
    /// </summary>
    /// <returns>
    /// >> Return of standardized information <<
    /// List<string> info >> General validation information
    /// List<string> warnings >> General validation warnings
    /// List<string> errors >> General validation errors
    /// List<string> errors >> Validation logs
    /// </returns>
    public GenInfoType ValidateConfiguration()
    {
      GenInfoType genInfo1 = new GenInfoType(), genInfo2 = new GenInfoType(), genInfoMerge = new GenInfoType();
      ReadConfigFromXml();

      /* Ensure a correct version */
      if (null == UartPduRXmlCfg)
      {
        genInfo1 = SetDefaultConfiguration();
      }

      /* Call Validator */
      Generator.ModuleValidator modVal = new Generator.ModuleValidator(UartPduRXmlCfg, pathToConfiguratioFile);
      genInfo2 = modVal.ValidateConfiguration();

      /* Merge outputs */
      genInfoMerge = OsarGenHelper.MergeGenInfoType(genInfo1, genInfo2);
      return genInfoMerge;
    }

    /// <summary>
    /// Used to generate the active configuration.
    /// </summary>
    /// <returns>
    /// >> Return of standardized information <<
    /// List<string> info >> General validation information
    /// List<string> warnings >> General validation warnings
    /// List<string> errors >> General validation errors
    /// List<string> errors >> Validation logs
    /// </returns>
    public GenInfoType GenerateConfiguration()
    {
      GenInfoType genInfo1 = new GenInfoType(), genInfo2 = new GenInfoType(), genInfoMerge = new GenInfoType();
      ReadConfigFromXml();

      /* Validate configuration */
      genInfo1 = ValidateConfiguration();

      /* If no validation error has been detected >> Start generation */
      if (0U == genInfo1.error.Count)
      {
        Generator.ModuleGenerator modGen = new Generator.ModuleGenerator(UartPduRXmlCfg, pathToConfiguratioFile, pathToModuleBaseFolder);
        genInfo2 = modGen.GenerateConfiguration();
      }

      /* Merge outputs */
      genInfoMerge = OsarGenHelper.MergeGenInfoType(genInfo1, genInfo2);

      return genInfoMerge;
    }

    /// <summary>
    /// Used to set the default configuration.
    /// </summary>
    /// <returns>
    /// >> Return of standardized information <<
    /// List<string> info >> General validation information
    /// List<string> warnings >> General validation warnings
    /// List<string> errors >> General validation errors
    /// List<string> errors >> Validation logs
    /// </returns>
    public GenInfoType SetDefaultConfiguration()
    {
      GenInfoType genInfo;
      Generator.DefaultCfgGenerator defGen = new Generator.DefaultCfgGenerator(UartPduRXmlCfg, pathToConfiguratioFile);
      genInfo = defGen.CreateDefaultConfiguration();
      SaveModuleConfigToXml();
      return genInfo;
    }

    /// <summary>
    /// Interface to update an outdated configuration to an up to date configuration version
    /// </summary>
    /// <returns>
    /// >> Return of standardized information <<
    /// List<string> info >> General validation information
    /// List<string> warnings >> General validation warnings
    /// List<string> errors >> General validation errors
    /// List<string> errors >> Validation logs
    /// </returns>
    public GenInfoType UpdateConfigurationFileVersion()
    {
      GenInfoType genInfo;
      ReadConfigFromXml();
      Generator.CfgVersionUpdater versUpdater = new Generator.CfgVersionUpdater(UartPduRXmlCfg, pathToConfiguratioFile);
      genInfo = versUpdater.UpdateCfgVersion();
      SaveModuleConfigToXml();
      return genInfo;
    }
    /// <summary>
    /// Interface to update an outdated configuration to an specific configuration version
    /// </summary>
    /// <param name="updateToVersion"> Xml file version to be updated </param>
    /// <returns>
    /// >> Return of standardized information <<
    /// List<string> info >> General validation information
    /// List<string> warnings >> General validation warnings
    /// List<string> errors >> General validation errors
    /// List<string> errors >> Validation logs</returns>
    public GenInfoType UpdateConfigurationFileVersion(XmlFileVersion updateToVersion)
    {
      GenInfoType genInfo;
      ReadConfigFromXml();
      Generator.CfgVersionUpdater versUpdater = new Generator.CfgVersionUpdater(UartPduRXmlCfg, pathToConfiguratioFile);
      genInfo = versUpdater.UpdateCfgVersion(updateToVersion);
      SaveModuleConfigToXml();
      return genInfo;
    }
    #endregion
  }
}
