﻿/*****************************************************************************************************************************
 * @file        DefaultWorkingDataBase.cs                                                                                    *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        06.04.2019                                                                                                   *
 * @brief       Implementation of the Default Working Data Base Creator Class                                                                   *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
* @addtogroup ModuleLibrary.Versions.v_1_0_3.Generator
* @{
*/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModuleLibrary.Versions.v_1_0_3.Models;
using OsarResources.Generator;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace ModuleLibrary.Versions.v_1_0_3.Generator
{
  internal class DefaultWorkingDataBase
  {
    private UartPduRWorkingDataBase xmlCfg;
    private GenInfoType info;

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="cfgFile"></param>
    public DefaultWorkingDataBase(UartPduRWorkingDataBase cfgFile)
    {
      xmlCfg = cfgFile;

      info.info = new List<string>();
      info.log = new List<string>();
      info.warning = new List<string>();
      info.error = new List<string>();
    }

    /// <summary>
    /// Interface to create the default working data base
    /// </summary>
    public GenInfoType CreateDefaultConfiguration()
    {
      info.AddInfoMsg(DefResources.WDB_LogMsg_StartDefaultWdbCreation + DefResources.WDB_FileMajorVersion +
        "." + DefResources.WDB_FileMinorVersion + "." + DefResources.WDB_FilePatchVersion);

      /* Create default configuration*/
      if (null == xmlCfg)
      {
        xmlCfg = new UartPduRWorkingDataBase();
      }

      // Create File Version
      xmlCfg.xmlFileVersion.MajorVersion = Convert.ToUInt16(DefResources.WDB_FileMajorVersion);
      xmlCfg.xmlFileVersion.MinorVersion = Convert.ToUInt16(DefResources.WDB_FileMinorVersion);
      xmlCfg.xmlFileVersion.PatchVersion = Convert.ToUInt16(DefResources.WDB_FilePatchVersion);

      /* Create data */
      xmlCfg.pduConfigList = new List<UartPduR_PduCfg>();
      UartPduR_PduCfg pduCfg = new UartPduR_PduCfg();
      pduCfg.dataLength = 0;
      pduCfg.moduleID = 255;
      pduCfg.pduDescription = "Default dummy PDU";
      pduCfg.pduId = 0;
      pduCfg.pduShortName = "DemoDummyPdu";
      pduCfg.rxNotificationFnc = "NULL_PTR";
      xmlCfg.pduConfigList.Add(pduCfg);

      return info;
    }
  }
}
