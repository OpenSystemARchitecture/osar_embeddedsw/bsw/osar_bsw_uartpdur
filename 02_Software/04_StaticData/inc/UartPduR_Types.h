/*****************************************************************************************************************************
 * @file        UartPduR_Types.h                                                                                             *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        25.03.2018 12:37:53                                                                                          *
 * @brief       Implementation of module global datatypes from the "UartPduR" module.                                        *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
 * @libNote     OSAR Source File Library v.0.1.2.15                                                                          *
*****************************************************************************************************************************/
#ifndef __UARTPDUR_TYPES_H
#define __UARTPDUR_TYPES_H

/**
 * @addtogroup OSAR_BSW 
 * @{
 */
/**
 * @addtogroup UartPduR
 * @{
 */

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 Start of include area                        << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#include "Std_Types.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                  End of include area                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of public data types                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/**
 * @brief           Available application errors of the UartPduR
 * @details         This enumeration implements all available application return and error values within this specific 
 *                  module. The Application error could be used as DET information or as return type of internal functions. 
 */
typedef enum {
  UARTPDUR_E_OK = 0,
  UARTPDUR_E_NOT_OK = 1,
  UARTPDUR_E_PENDING = 2,
  UARTPDUR_E_NOT_IMPLEMENTED = 3,
  UARTPDUR_E_GENERIC_PROGRAMMING_FAILURE = 4,
  UARTPDUR_E_ID_UNKNOWN = 100,
  UARTPDUR_E_NULL_POINTER = 101,
  UARTPDUR_E_INVALID_PDU_LENGTH = 102,
  UARTPDUR_E_TRANSMITBUFFER_OVERFLOW = 103,
  UARTPDUR_E_NOTIFICATION_FNC_NOT_AVAILABLE = 104,
  UARTPDUR_E_CRC_FAILURE = 105,
}UartPduR_ReturnType;

/**
 * @brief           Available application return values of the UartPduR
 * @details         Redefinition of the UartPduR_ReturnType as error type.
 */
typedef UartPduR_ReturnType UartPduR_ErrorType;

/**
* @brief           General PDU Configuration Type
*/
typedef struct
{
  uint8   moduleID;                             /*!< Module Id wherer the pdu is used */
  uint16  pduId;                                /*!< ID of the PDU */
  uint16  dataLength;                           /*!< Length of used data within the PDU >> Without frame end pattern */
  void(*rxNotificationFnc)(uint8*, uint16);     /*!< Function pointer to the higher layer module notification function if rx data has bee received */
}UartPduR_PduConfigType;



/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of public data types                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            Start of public definition area                   << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of public definition area                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of global variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of global variables                      << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>           Start of public function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of public function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/**
 * @}
 */
/**
 * @}
 */

#endif /* __UARTPDUR_TYPES_H*/
