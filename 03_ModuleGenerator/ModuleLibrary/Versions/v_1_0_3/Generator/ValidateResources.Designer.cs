﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ModuleLibrary.Versions.v_1_0_3.Generator {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "16.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class ValidateResources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal ValidateResources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("ModuleLibrary.Versions.v_1_0_3.Generator.ValidateResources", typeof(ValidateResources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Input data base files validated.
        /// </summary>
        internal static string LogMsg_InputDataBaseCfgValidationDone {
            get {
                return ResourceManager.GetString("LogMsg_InputDataBaseCfgValidationDone", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Module configuration file validated.
        /// </summary>
        internal static string LogMsg_ModuleCfgValidationDone {
            get {
                return ResourceManager.GetString("LogMsg_ModuleCfgValidationDone", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Start validation of the input data base files.
        /// </summary>
        internal static string LogMsg_StartInputDataBaseCfgValidation {
            get {
                return ResourceManager.GetString("LogMsg_StartInputDataBaseCfgValidation", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Start validation of the module configuration file.
        /// </summary>
        internal static string LogMsg_StartModuleCfgValidation {
            get {
                return ResourceManager.GetString("LogMsg_StartModuleCfgValidation", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Start validation of the working data base file.
        /// </summary>
        internal static string LogMsg_StartWorkingDataBaseCfgValidation {
            get {
                return ResourceManager.GetString("LogMsg_StartWorkingDataBaseCfgValidation", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Working data base files validated.
        /// </summary>
        internal static string LogMsg_WorkingDataBaseCfgValidationDone {
            get {
                return ResourceManager.GetString("LogMsg_WorkingDataBaseCfgValidationDone", resourceCulture);
            }
        }
    }
}
