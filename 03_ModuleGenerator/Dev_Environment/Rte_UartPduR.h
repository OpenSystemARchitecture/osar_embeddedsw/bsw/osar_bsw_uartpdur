#pragma once

#define Rte_ErrorType_PpGeneralUartPduR_UartPduR_SendUartPdu_NullPointer               100
#define Rte_ErrorType_PpGeneralUartPduR_UartPduR_SendUartPdu_InvalidPduLength          101
#define Rte_ErrorType_PpGeneralUartPduR_UartPduR_SendUartPdu_UnknownId                 102
#define Rte_ErrorType_PpGeneralUartPduR_UartPduR_SendUartPdu_TransmitBufferOverflow    103
#define Rte_ErrorType_PpGeneralUartPduR_UartPduR_SendUartPdu_GenericProgrammingFailure 104