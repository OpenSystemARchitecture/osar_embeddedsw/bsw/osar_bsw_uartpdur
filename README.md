# OSAR_BSW_UartPduR

## General
The Uart Pdu Router Module (UartPduR) is an abstraction module of the Uart Interface module. It shall be used to distribute Uart Frames to different Software modules.

## Abbreviations:
OSAR == Open System ARchitecture
UartPduR == Uart Package-data-unit-Router

## Useful Links:
- [Overall OSAR-Artifactory](http://riddiks.ddns.net:8082/artifactory/webapp/#/artifacts/browse/tree/General/prj-open-system-architecture)
- [OSAR - UartPduR releases](http://riddiks.ddns.net:8082/artifactory/list/prj-open-system-architecture/Osar_EmbeddedSW/BSW/OSAR_UartPduR/)
- [OSAR - Documents](http://riddiks.ddns.net:8082/artifactory/list/prj-open-system-architecture/Osar_Documents/)
- [OSAR - Hyperspace](http://riddiks.ddns.net:8082/artifactory/list/prj-open-system-architecture/Osar_Hyperspace/)
  - Main-Tool to setup an OSAR Embedded Software Project.
- [OSAR - Tools](http://riddiks.ddns.net:8082/artifactory/list/prj-open-system-architecture/Osar_Tools/)
