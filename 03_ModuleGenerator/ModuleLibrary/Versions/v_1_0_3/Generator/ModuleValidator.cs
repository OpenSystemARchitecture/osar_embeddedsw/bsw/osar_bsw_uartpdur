﻿/*****************************************************************************************************************************
 * @file        ModuleValidator.cs                                                                                           *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        06.04.2019                                                                                                   *
 * @brief       Implementation of the Module Validation Class                                                                *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
* @addtogroup ModuleLibrary.Versions.v_1_0_3.Generator
* @{
*/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OsarResources.Generator;
using OsarResources.XML;
using OsarResources.Generator.Resources;
using System.IO;
using System.Xml.Serialization;
using ModuleLibrary.Versions.v_1_0_3.Models;
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace ModuleLibrary.Versions.v_1_0_3.Generator
{
  internal class ModuleValidator
  {
    private Models.UartPduRXml xmlCfg;
    private string pathToConfiguratioFile;  // Including config file name
    private string configFilePath;          // Includes only path
    private GenInfoType info;

    List<UartPduInputDatabase> uartPduInputDataBase = new List<UartPduInputDatabase>();

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="cfgFile"> Config file which shall be generated </param>
    /// <param name="pathToCfgFile"> Path of Configuration file </param>
    public ModuleValidator(Models.UartPduRXml cfgFile, string pathToCfgFile)
    {
      xmlCfg = cfgFile;
      pathToConfiguratioFile = pathToCfgFile;
      configFilePath = Path.GetDirectoryName(pathToCfgFile);

      info.info = new List<string>();
      info.log = new List<string>();
      info.warning = new List<string>();
      info.error = new List<string>();
    }

    /// <summary>
    /// Interface to validate the configuration
    /// </summary>
    /// <returns> Generation information </returns>
    public GenInfoType ValidateConfiguration()
    {
      /* Start validation of the module configuration file */
      validateModuleCfgFile();

      /* Start validation of the input data base file */
      if(null == xmlCfg.pathOfInputDataBases)
      {
        info.AddErrorMsg(Convert.ToUInt16(GenErrorWarningCodes.Error_1003), GenErrorWarningCodes.Error_1003_Msg);
      }
      else
      {
        validateInputDataBase();
      }

      /* Validate working data base */
      validateWorkingDataBase();


      return info;
    }

    /// <summary>
    /// Helper function to validate the module configuration file
    /// </summary>
    private void validateModuleCfgFile()
    {
      info.AddLogMsg(ValidateResources.LogMsg_StartModuleCfgValidation);

      /* Validate configuration file version */
      XmlFileVersion defaultVersion = new XmlFileVersion();
      defaultVersion.MajorVersion = Convert.ToUInt16(DefResources.CfgFileMajorVersion);
      defaultVersion.MinorVersion = Convert.ToUInt16(DefResources.CfgFileMinorVersion);
      defaultVersion.PatchVersion = Convert.ToUInt16(DefResources.CfgFilePatchVersion);
      if (false == OsarResources.Generic.OsarGenericHelper.XmlFileVersionEqual(defaultVersion, xmlCfg.xmlFileVersion))
      {
        info.AddErrorMsg(Convert.ToUInt16(ErrorWarningCodes.Error_0000), ErrorWarningCodes.Error_0000_Msg);
      }

      /* Check mainfunction cycle time is valid */
      if(0U == xmlCfg.miBMainfunctionCycleTimeMs)
      {
        info.AddErrorMsg(Convert.ToUInt16(GenErrorWarningCodes.Error_1000), GenErrorWarningCodes.Error_1000_Msg);
      }

      /* Check max frame size */
      if(0U == xmlCfg.uartIfMaxFrameDataSize)
      {
        info.AddErrorMsg(Convert.ToUInt16(GenErrorWarningCodes.Error_1001), GenErrorWarningCodes.Error_1001_Msg);
      }

      /* Check if frame end pattern is set */
      if(null == xmlCfg.uartFrameEndPatternElements)
      {
        info.AddErrorMsg(Convert.ToUInt16(GenErrorWarningCodes.Error_1002), GenErrorWarningCodes.Error_1002_Msg);
      }
      else
      {
        /* Check if frame end pattern is valid */
        if (0U == xmlCfg.uartFrameEndPatternElements.Length)
        {
          info.AddErrorMsg(Convert.ToUInt16(GenErrorWarningCodes.Error_1002), GenErrorWarningCodes.Error_1002_Msg);
        }
      }

      info.AddLogMsg(ValidateResources.LogMsg_ModuleCfgValidationDone);
    }


    /// <summary>
    /// Helper function to validate the input data base file
    /// </summary>
    /// <param name="pathToInputDataBaseFile"></param>
    private void validateInputDataBase()
    {
      List<UInt16> listWithUsedIds = new List<UInt16>();
      
      info.AddLogMsg(ValidateResources.LogMsg_StartInputDataBaseCfgValidation);

      /* Parsing all input data bases */
      foreach (string inputDataBaseFile in xmlCfg.pathOfInputDataBases)
      {
        string inputDataBaseFilePath = configFilePath + inputDataBaseFile;
        if (!File.Exists(inputDataBaseFilePath))
        {
          info.AddErrorMsg(Convert.ToUInt16(GenErrorWarningCodes.Error_1004), GenErrorWarningCodes.Error_1004_Msg + inputDataBaseFile);
        }
        else
        {
          /* Parsing input data base */
          UartPduInputDatabase tempInputDataBase = new UartPduInputDatabase();
          XmlSerializer reader = new XmlSerializer(tempInputDataBase.GetType());
          StreamReader file = new StreamReader(inputDataBaseFilePath);
          tempInputDataBase = (UartPduInputDatabase)reader.Deserialize(file);
          file.Close();
          uartPduInputDataBase.Add(tempInputDataBase);

          /* ==================== Validate active input data base ====================*/
          /* Validate configuration file version */
          XmlFileVersion defaultVersion = new XmlFileVersion();
          defaultVersion.MajorVersion = Convert.ToUInt16(DefResources.IDP_DefaultVersionMajor);
          defaultVersion.MinorVersion = Convert.ToUInt16(DefResources.IDP_DefaultVersionMinor);
          defaultVersion.PatchVersion = Convert.ToUInt16(DefResources.IDP_DefaultVersionPatch);
          if (false == OsarResources.Generic.OsarGenericHelper.XmlFileVersionEqual(defaultVersion, tempInputDataBase.xmlFileVersion))
          {
            info.AddErrorMsg(Convert.ToUInt16(ErrorWarningCodes.Error_0000), ErrorWarningCodes.Error_0000_Msg +
              ">> Input data base file version: " +
                tempInputDataBase.xmlFileVersion.MajorVersion.ToString() + "." +
                tempInputDataBase.xmlFileVersion.MinorVersion.ToString() + "." +
                tempInputDataBase.xmlFileVersion.PatchVersion.ToString() +
              "\n>> Supported input data base file version: " +
                DefResources.IDP_DefaultVersionMajor + "." +
                DefResources.IDP_DefaultVersionMinor + "." +
                DefResources.IDP_DefaultVersionPatch +
              "\n>> Input data base file name: " + inputDataBaseFile);
          }

          /* Check data base content */
          for (int elementIndex = 0; elementIndex < tempInputDataBase.pduConfigList.Length; elementIndex++)
          {
            listWithUsedIds.Add(tempInputDataBase.pduConfigList[elementIndex].pduId);

            /* Check Pdu Length */
            if (0U == tempInputDataBase.pduConfigList[elementIndex].dataLength)
            {
              info.AddErrorMsg(Convert.ToUInt16(GenErrorWarningCodes.Error_1005), GenErrorWarningCodes.Error_1005_Msg);
            }

            /* Check for valid pdu id*/
            if (( true == tempInputDataBase.pduConfigList[elementIndex].pduShortName.Contains(" ") ) ||
              ( true == tempInputDataBase.pduConfigList[elementIndex].pduShortName.Contains(";") ) ||
              ( true == tempInputDataBase.pduConfigList[elementIndex].pduShortName.Contains(".") ) ||
              ( true == tempInputDataBase.pduConfigList[elementIndex].pduShortName.Contains("*") ) ||
              ( true == tempInputDataBase.pduConfigList[elementIndex].pduShortName.Contains("/") ) ||
              ( true == tempInputDataBase.pduConfigList[elementIndex].pduShortName.Contains("\\") ))
            {
              info.AddErrorMsg(Convert.ToUInt16(GenErrorWarningCodes.Error_1006), GenErrorWarningCodes.Error_1006_Msg);
            }
          }
        }
      }

      /* Check if there are duplicated pdu Ids */
      var duplicates = listWithUsedIds.GroupBy(s => s).SelectMany(grp => grp.Skip(1));
      listWithUsedIds = duplicates.ToList();
      if (listWithUsedIds.Count() > 0)
      {
        string temp = "Duplicated PDU IDs: ";
        for (int idx = 0; idx < listWithUsedIds.Count(); idx++)
        {
          temp += listWithUsedIds[idx].ToString() + ", ";
        }

        info.AddErrorMsg(Convert.ToUInt16(GenErrorWarningCodes.Error_1007), GenErrorWarningCodes.Error_1007_Msg + temp);
      }

      info.AddLogMsg(ValidateResources.LogMsg_InputDataBaseCfgValidationDone);
    }

    /// <summary>
    /// Helper function to validate the working data base file
    /// </summary>
    private void validateWorkingDataBase()
    {
      info.AddLogMsg(ValidateResources.LogMsg_StartWorkingDataBaseCfgValidation);
      if (!File.Exists(configFilePath + xmlCfg.pathOfWorkingDataBase + DefResources.WDB_WorkingDataBaseName))
      {
        info.AddWaringMsg(Convert.ToUInt16(GenErrorWarningCodes.Warning_1001), GenErrorWarningCodes.Warning_1001_Msg);
      }
      else
      {

        UartPduRWorkingDataBase workingDataBase = new UartPduRWorkingDataBase();

        XmlSerializer reader = new XmlSerializer(workingDataBase.GetType());
        StreamReader file = new StreamReader(configFilePath + xmlCfg.pathOfWorkingDataBase + DefResources.WDB_WorkingDataBaseName);
        workingDataBase = (UartPduRWorkingDataBase)reader.Deserialize(file);
        file.Close();

        /* Validate configuration file version */
        XmlFileVersion defaultVersion = new XmlFileVersion();
        defaultVersion.MajorVersion = Convert.ToUInt16(DefResources.IDP_DefaultVersionMajor);
        defaultVersion.MinorVersion = Convert.ToUInt16(DefResources.IDP_DefaultVersionMinor);
        defaultVersion.PatchVersion = Convert.ToUInt16(DefResources.IDP_DefaultVersionPatch);
        if (false == OsarResources.Generic.OsarGenericHelper.XmlFileVersionEqual(defaultVersion, workingDataBase.xmlFileVersion))
        {
          info.AddErrorMsg(Convert.ToUInt16(ErrorWarningCodes.Error_0000), ErrorWarningCodes.Error_0000_Msg +
            ">> Working data base file version: " +
              workingDataBase.xmlFileVersion.MajorVersion.ToString() + "." +
              workingDataBase.xmlFileVersion.MinorVersion.ToString() + "." +
              workingDataBase.xmlFileVersion.PatchVersion.ToString() +
            "\n>> Supported working data base file version: " +
              DefResources.WDB_FileMajorVersion + "." +
              DefResources.WDB_FileMinorVersion + "." +
              DefResources.WDB_FilePatchVersion);
        }

        /* Start validation for each pdu data element */
        for (int idx = 0; idx < workingDataBase.pduConfigList.Count(); idx++)
        {
          /* Check Pdu Length */
          if (0 == workingDataBase.pduConfigList[idx].dataLength)
          {
            info.AddErrorMsg(Convert.ToUInt16(GenErrorWarningCodes.Error_1005), GenErrorWarningCodes.Error_1005_Msg);
          }

          if (( true == workingDataBase.pduConfigList[idx].rxNotificationFnc.Contains(" ") ) ||
              ( true == workingDataBase.pduConfigList[idx].rxNotificationFnc.Contains(";") ) ||
              ( true == workingDataBase.pduConfigList[idx].rxNotificationFnc.Contains(".") ) ||
              ( true == workingDataBase.pduConfigList[idx].rxNotificationFnc.Contains("*") ) ||
              ( true == workingDataBase.pduConfigList[idx].rxNotificationFnc.Contains("/") ) ||
              ( true == workingDataBase.pduConfigList[idx].rxNotificationFnc.Contains("\\") ))
          {
            info.AddErrorMsg(Convert.ToUInt16(GenErrorWarningCodes.Error_1006), GenErrorWarningCodes.Error_1006_Msg + workingDataBase.pduConfigList[idx].pduId);
          }

          if (( true == workingDataBase.pduConfigList[idx].pduShortName.Contains(" ") ) ||
              ( true == workingDataBase.pduConfigList[idx].pduShortName.Contains(";") ) ||
              ( true == workingDataBase.pduConfigList[idx].pduShortName.Contains(".") ) ||
              ( true == workingDataBase.pduConfigList[idx].pduShortName.Contains("*") ) ||
              ( true == workingDataBase.pduConfigList[idx].pduShortName.Contains("/") ) ||
              ( true == workingDataBase.pduConfigList[idx].pduShortName.Contains("\\") ))
          {
            info.AddErrorMsg(Convert.ToUInt16(GenErrorWarningCodes.Error_1008), GenErrorWarningCodes.Error_1008_Msg + workingDataBase.pduConfigList[idx].pduId);
          }
        }
      }

      info.AddLogMsg(ValidateResources.LogMsg_WorkingDataBaseCfgValidationDone);
    }
  }
}
