﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OsarResources.XML;
using OsarResources.Generic;
using System.Xml.Serialization;

namespace UartPduR_WorkingDataBaseXsd
{
  public struct UartPduR_PduCfg
  {
    public Byte moduleID;                       /*!< Module Id where the Pdu is used */
    public UInt16 pduId;                        /*!< ID of the PDU */
    public UInt16 dataLength;                   /*!< Length of used data within the PDU >> Without frame end pattern */
    public String rxNotificationFnc;            /*!< Function pointer to the higher layer module notification function if rx data has bee received */
    public String pduShortName;                 /*!< Sort name of the PDU */
    public String pduDescription;               /*!< Unused Pdu Description */
  }

  public class UartPduRWorkingDataBase
  {
    public XmlFileVersion xmlFileVersion;
    public List<UartPduR_PduCfg> pduConfigList;
  }
}
