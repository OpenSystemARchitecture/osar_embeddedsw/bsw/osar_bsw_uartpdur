﻿/*****************************************************************************************************************************
 * @file        DefaultCfg.cs                                                                                                *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        06.04.2019                                                                                                   *
 * @brief       Implementation of the Default Module Class                                                                   *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
* @addtogroup ModuleLibrary.Versions.v_1_0_3.Generator
* @{
*/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OsarResources.Generator;
using OsarResources.Generic;
using OsarResources.XML;
using ModuleLibrary.Versions.v_1_0_3.Models;
using System.IO;
using System.Xml.Serialization;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace ModuleLibrary.Versions.v_1_0_3.Generator
{
  internal class DefaultCfgGenerator
  {
    private Models.UartPduRXml xmlCfg;
    private string pathToConfiguratioFile;  // Including config file name
    private string configFilePath;          // Includes only path
    private GenInfoType info;

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="cfgFile"> Config file which shall be set to default </param>
    /// <param name="pathToCfgFile"> Path of Configuration file </param>
    public DefaultCfgGenerator(Models.UartPduRXml cfgFile, string pathToCfgFile)
    {
      xmlCfg = cfgFile;
      pathToConfiguratioFile = pathToCfgFile;
      configFilePath = Path.GetDirectoryName(pathToCfgFile);

      info.info = new List<string>();
      info.log = new List<string>();
      info.warning = new List<string>();
      info.error = new List<string>();
    }

    /// <summary>
    /// Interface to set default configuration
    /// </summary>
    /// <returns> Generation information </returns>
    public GenInfoType CreateDefaultConfiguration()
    {
      info.AddInfoMsg(DefResources.InfoMsg_StartDefaultCfgCreation + DefResources.CfgFileMajorVersion +
        "." + DefResources.CfgFileMinorVersion + "." + DefResources.CfgFilePatchVersion);
      info.AddLogMsg(DefResources.LogMsg_StartDefaultCfgCreation);

      /* Create default configuration*/
      if (null == xmlCfg)
      {
        xmlCfg = new Models.UartPduRXml();
      }

      // Create Configuration File Version
      xmlCfg.xmlFileVersion.MajorVersion = Convert.ToUInt16(DefResources.CfgFileMajorVersion);
      xmlCfg.xmlFileVersion.MinorVersion = Convert.ToUInt16(DefResources.CfgFileMinorVersion);
      xmlCfg.xmlFileVersion.PatchVersion = Convert.ToUInt16(DefResources.CfgFilePatchVersion);

      // Create Det Module Id
      xmlCfg.detModuleID = Convert.ToUInt16(DefResources.DefaultModuleId);

      // Create module Det usage state
      if (DefResources.DefaultModuleUsage == SystemState.STD_ON.ToString())
        xmlCfg.detModuleUsage = SystemState.STD_ON;
      else
        xmlCfg.detModuleUsage = SystemState.STD_OFF;

      /* Set default Frame End Pattern */
      xmlCfg.uartFrameEndPatternElements = new Byte[2];
      xmlCfg.uartFrameEndPatternElements[0] = 0x0D;
      xmlCfg.uartFrameEndPatternElements[1] = 0x0A;

      /* Set default input data base path and name */
      xmlCfg.pathOfInputDataBases = new List<string>();
      xmlCfg.pathOfInputDataBases.Add(DefResources.DefaultXmlInputDataBasePath + DefResources.IDP_DefaultDataBaseName);

      /* Set default working data base path */
      xmlCfg.pathOfWorkingDataBase = DefResources.DefaultXmlWorkingDataBasePath;

      /* Set default max data elements */
      xmlCfg.uartIfMaxFrameDataSize = Convert.ToUInt16(DefResources.DefaultMaxDataElements);

      if (DefResources.DefaultCrc32EthernetE2ECommunication == SystemState.STD_OFF.ToString())
      {
        xmlCfg.useCrc32EthernetE2ECommunication = SystemState.STD_OFF;
      }
      else
      {
        xmlCfg.useCrc32EthernetE2ECommunication = SystemState.STD_ON;
      }

      /* Set Mainfunction Cycle Time */
      xmlCfg.miBMainfunctionCycleTimeMs = Convert.ToUInt16(DefResources.DefaultMainfunctionCycleTimeMs);


      /* Create a default input data base file */
      createDefaultInputDataBase();

      return info;
    }

    /// <summary>
    /// Helper function to create a default input data base
    /// </summary>
    private void createDefaultInputDataBase()
    { 
      if(!File.Exists(configFilePath + xmlCfg.pathOfInputDataBases[0]))
      {
        info.AddWaringMsg(Convert.ToUInt16(GenErrorWarningCodes.Warning_1000), GenErrorWarningCodes.Warning_1000_Msg);
        info.AddInfoMsg("Do not use the default input data base file in productive code!");

        UartPduInputDatabase inputDataBase = new UartPduInputDatabase();

        /* Create file version */
        XmlFileVersion fileVersion = new XmlFileVersion();
        fileVersion.MajorVersion = Convert.ToUInt16(DefResources.IDP_DefaultVersionMajor);
        fileVersion.MinorVersion = Convert.ToUInt16(DefResources.IDP_DefaultVersionMinor);
        fileVersion.PatchVersion = Convert.ToUInt16(DefResources.IDP_DefaultVersionPatch);
        inputDataBase.xmlFileVersion = fileVersion;

        /* Create dummy pdu config */
        Uart_PduCfg dummyPduCfg = new Uart_PduCfg();
        UartSubDataSet dummySubDataSet = new UartSubDataSet();

        dummyPduCfg.pduId = 0;
        dummyPduCfg.dataLength = 1;
        dummyPduCfg.pduShortName = "DummyPdu";
        dummyPduCfg.pduDescription = "This is a dummy PDU. Replace this pdu with an valid one.";

        dummySubDataSet.dataStartIndex = 0;
        dummySubDataSet.dataLength = 1;
        dummySubDataSet.dataName = "DummyPduByteLoad";
        dummySubDataSet.dataDescription = "This is a dummy PDU load with the size of one Byte.";

        inputDataBase.pduConfigList = new Uart_PduCfg[1];
        inputDataBase.pduConfigList[0] = dummyPduCfg;

        dummyPduCfg.pduDataSet = new UartSubDataSet[1];
        dummyPduCfg.pduDataSet[0] = dummySubDataSet;

        /* Safe input data base */
        info.AddInfoMsg("Store default input data base to: " + configFilePath + xmlCfg.pathOfInputDataBases[0] );
        XmlSerializer writer = new XmlSerializer(inputDataBase.GetType());
        StreamWriter file = new StreamWriter(configFilePath + xmlCfg.pathOfInputDataBases[0]);
        writer.Serialize(file, inputDataBase);
        file.Close();
      }
    }
  }
}
