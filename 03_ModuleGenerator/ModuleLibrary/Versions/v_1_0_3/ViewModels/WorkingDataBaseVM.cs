﻿/*****************************************************************************************************************************
 * @file        WorkingDataBaseVM.cs                                                                                         *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        29.09.2019                                                                                                   *
 * @brief       Implementation of the View Model from the Working Data Base Xml Cfg Cass                                     *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
 * @addtogroup ModuleLibrary.Versions.v_1_0_3.ViewModels
 * @{
 */

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using ModuleLibrary.Versions.v_1_0_3.Models;
using ModuleLibrary.Versions.v_1_0_3.Generator;
using PropertyChanged;
using System.ComponentModel;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace ModuleLibrary.Versions.v_1_0_3.ViewModels
{
  internal class WorkingDataBaseVM : INotifyPropertyChanged
  {
    private static UartPduRWorkingDataBase UartPduRWorkingdataBaseXml = new UartPduRWorkingDataBase();
    private static string pathToWorkingDataBaseFile;
    private static StoreConfiguration storeCfgWDB = new StoreConfiguration(WorkingDataBaseVM.SaveWorkingDataBaseToXml);


    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="pathToDataBase"></param>
    public WorkingDataBaseVM(string pathToDataBase)
    {
      pathToWorkingDataBaseFile = pathToDataBase;
      pathToWorkingDataBaseFile += ".\\" + DefResources.WDB_WorkingDataBaseName;

      if (false == File.Exists(pathToWorkingDataBaseFile))
      {
        DefaultWorkingDataBase defGen = new DefaultWorkingDataBase(UartPduRWorkingdataBaseXml);
        defGen.CreateDefaultConfiguration();
      }
      else
      {
        /* Read Working Data Base File */
        ReadConfigFromXml();
      }
    }

    #region INotifyPropertyChanged Interface implementation
    /// <summary>
    /// The event that is fired when any child property changes its value
    /// </summary>
    public event PropertyChangedEventHandler PropertyChanged = (sender, e) =>
    {
      SaveWorkingDataBaseToXml();
    };

    /// <summary>
    /// Encapsulation of property changed event
    /// </summary>
    /// <param name="e"></param>
    public virtual void OnPropertyChanged(PropertyChangedEventArgs e)
    {
      if (null != PropertyChanged)
      {
        PropertyChanged(this, e);
      }
    }
    #endregion

    #region Attributes
    /// <summary>
    /// Interface for the working data base
    /// </summary>
    public UartPduRWorkingDataBase UartPduRWorkingdataBaseXmlFile 
    { 
      get => UartPduRWorkingdataBaseXml; 
      set => UartPduRWorkingdataBaseXml = value; 
    }
    #endregion


    #region Configuration Load and Storage
    /*
     * @brief       Function to store the current working data base to an xml file
     * @param       none
     * @retval      none
     */
    private static void SaveWorkingDataBaseToXml()
    {
      XmlSerializer writer = new XmlSerializer(UartPduRWorkingdataBaseXml.GetType());
      StreamWriter file = new StreamWriter(pathToWorkingDataBaseFile);
      writer.Serialize(file, UartPduRWorkingdataBaseXml);
      file.Close();
    }

    /*
     * @brief       Function to read the current working data base from an xml file
     * @param       none
     * @retval      none
     */
    private static void ReadConfigFromXml()
    {
      XmlSerializer reader = new XmlSerializer(UartPduRWorkingdataBaseXml.GetType());
      StreamReader file = new StreamReader(pathToWorkingDataBaseFile);
      UartPduRWorkingdataBaseXml = (UartPduRWorkingDataBase)reader.Deserialize(file);
      file.Close();
    }
    #endregion
  }
}
