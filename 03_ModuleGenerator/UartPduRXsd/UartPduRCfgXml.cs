﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OsarResources.XML;
using OsarResources.Generic;
using System.Xml.Serialization;
using System.IO;

namespace UartPduRXsd
{
  public class UartPduRCfgXml
  {
    public XmlFileVersion xmlFileVersion;
    public UInt16 detModuleID;
    public SystemState detModuleUsage;

    public UInt16 miBMainfunctionCycleTimeMs;

    public SystemState useCrc32EthernetE2ECommunication;
    public UInt16 uartIfMaxFrameDataSize;
    public List<String> pathOfInputDataBases;
    public String pathOfWorkingDataBase;
    [XmlElement(DataType = "hexBinary")]
    public Byte[] uartFrameEndPatternElements;
  }
}
