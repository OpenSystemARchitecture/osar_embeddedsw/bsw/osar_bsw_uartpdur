/*****************************************************************************************************************************
 * @file        UartPduR_PBCfg.c                                                                                             *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        25.03.2018 12:37:53                                                                                          *
 * @brief       Generated source file data of the UartPduR module.                                                           *
 * @details     Implementation of general generated data.                                                                    *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
 * @libNote     OSAR Source File Library v.0.1.2.15                                                                          *
*****************************************************************************************************************************/
/**
 * @addtogroup UartPduR
 * @{
 */

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 Start of include area                        << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#include "Det.h"
#include "UartPduR_PBCfg.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                  End of include area                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of private data types                   << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of private data types                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            Start of private definitions area                 << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of private definitions area                  << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of global variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/*------------------------------------------- Map variables into constand memory -------------------------------------------*/
#define UartPduR_START_SEC_CONST
#include "UartPduR_MemMap.h"
#define UartPduR_STOP_SEC_CONST
#include "UartPduR_MemMap.h"
/*---------------------------------------- Map variables into uninitialized memory ----------------------------------------*/
#define UartPduR_START_SEC_NOINIT_VAR
#include "UartPduR_MemMap.h"
#define UartPduR_STOP_SEC_NOINIT_VAR
#include "UartPduR_MemMap.h"
/*----------------------------------------- Map variables into initialized memory -----------------------------------------*/
#define UartPduR_START_SEC_INIT_VAR
#include "UartPduR_MemMap.h"
#define UartPduR_STOP_SEC_INIT_VAR
#include "UartPduR_MemMap.h"
/*--------------------------------------- Map variables into zero initialized memory ---------------------------------------*/
#define UartPduR_START_SEC_ZERO_INIT_VAR
#include "UartPduR_MemMap.h"
#define UartPduR_STOP_SEC_ZERO_INIT_VAR
#include "UartPduR_MemMap.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of global variables                      << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of private variables                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/*------------------------------------------- Map variables into constand memory -------------------------------------------*/
#define UartPduR_START_SEC_CONST
#include "UartPduR_MemMap.h"
/**
* @brief           General Uart pdu router >> pdu configuration data
*/
const UartPduR_PduConfigType uartPduR_PduConfigList[UARTPDUR_CNT_OF_CONFIGURED_PDUS] = {
  { 0, 0, 10, &testReceiveFncPduId0 },
  { 0, 1, 10,&testReceiveFncPduId1 },
  { 0, 0x050A, 10, &testReceiveFncPduId4 },
  { 0, 5, 10, NULL_PTR }
};

/**
* @brief           General Uart pdu router >> pdu  frame end pattern
*/
const uint8 pduEndPattern[UARTPDUR_CNT_PDU_END_PATTERN] = { 0x0D, 0x0A };

/**
* @brief           Used Uart Module Ids
*/
const uint8 listOfUsedUartModules[UARTPDUR_CNT_OF_USED_UART_MODULES] = { 0 };
#define UartPduR_STOP_SEC_CONST
#include "UartPduR_MemMap.h"
/*---------------------------------------- Map variables into uninitialized memory ----------------------------------------*/
#define UartPduR_START_SEC_NOINIT_VAR
#include "UartPduR_MemMap.h"
#define UartPduR_STOP_SEC_NOINIT_VAR
#include "UartPduR_MemMap.h"
/*----------------------------------------- Map variables into initialized memory -----------------------------------------*/
#define UartPduR_START_SEC_INIT_VAR
#include "UartPduR_MemMap.h"
#define UartPduR_STOP_SEC_INIT_VAR
#include "UartPduR_MemMap.h"
/*--------------------------------------- Map variables into zero initialized memory ---------------------------------------*/
#define UartPduR_START_SEC_ZERO_INIT_VAR
#include "UartPduR_MemMap.h"
#define UartPduR_STOP_SEC_ZERO_INIT_VAR
#include "UartPduR_MemMap.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of private variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>           Start of private function prototypes               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            End of private function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>        Start of private function implementation              << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/*--------------------------------------------- Map function into code memory ---------------------------------------------*/
#define UartPduR_START_SEC_CODE
#include "UartPduR_MemMap.h"
#define UartPduR_STOP_SEC_CODE
#include "UartPduR_MemMap.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>         End of private function implementation               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>          Start of public function implementation             << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/*--------------------------------------------- Map function into code memory ---------------------------------------------*/
#define UartPduR_START_SEC_CODE
#include "UartPduR_MemMap.h"
#define UartPduR_STOP_SEC_CODE
#include "UartPduR_MemMap.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>          End of public function implementation               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/**
 * @}
 */
