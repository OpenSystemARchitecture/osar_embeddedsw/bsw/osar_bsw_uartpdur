/******************************************************************************
* @file    main.c
* @author  Reinemuth Sebastian
* @date    17-02-2016
* @brief   main-Functions
* last checkin :
* $Author: sreinemuth $ @ $Date: 2017-02-18 12:31:21 +0100 (Sa, 18 Feb 2017) $ , Revision: $Rev: 14 $
*****************************************************************************/

/* Private includes ----------------------------------------------------------*/
#include "Std_Types.h"
#include "UartPduR.h"
/* Global variables ----------------------------------------------------------*/

/* Private function prototypes -----------------------------------------------*/

void dummyTestFnc(uint8 *data, uint16 cnt)
{

}

/* Functions -----------------------------------------------------------------*/
int main()
{
  uint8 dummyData[4] = { 0xAA, 0xBB , 0xCC, 0xDD};
  UartPduR_InitMemory();
  UartPduR_Init();

  UartPduR_SendUartPdu_0_DummyPdu(&dummyData[0]);

  UartPduR_Mainfunction();

  while (1);
}