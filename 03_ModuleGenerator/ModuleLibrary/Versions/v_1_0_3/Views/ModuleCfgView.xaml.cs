﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ModuleLibrary.Versions.v_1_0_3.ViewModels;

namespace ModuleLibrary.Versions.v_1_0_3.Views
{
  /// <summary>
  /// Interaction logic for the ModuleCfgView.xaml
  /// </summary>
  public partial class ModuleCfgView : UserControl
  {
    ViewModels.Module_ViewModel newCfg;
    string pathToConfigFile;
    string absPathModuleBaseFolder;

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="pathToCfgFile">Absolute Path to configuration file</param>
    /// <param name="absPathToModuleBaseFolder">Absolute Path to module base folder</param>
    public ModuleCfgView(string pathToCfgFile, string absPathToModuleBaseFolder)
    {
      /* Check if config file path is an rooted one */
      if (true == System.IO.Path.IsPathRooted(pathToCfgFile))
      {
        pathToConfigFile = pathToCfgFile;
      }
      else
      {
        pathToConfigFile = absPathToModuleBaseFolder + pathToCfgFile;
      }

      newCfg = new ViewModels.Module_ViewModel(pathToConfigFile, absPathToModuleBaseFolder);
      absPathModuleBaseFolder = absPathToModuleBaseFolder;

      InitializeComponent();

      /* Set Data Context where data is binded to*/
      this.DataContext = newCfg;
    }

    /// <summary>
    /// Button for the working data base selector
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void B_SelectWorkingDataBaseFilePath_Click(object sender, RoutedEventArgs e)
    {
      using (var fbd = new System.Windows.Forms.FolderBrowserDialog())
      {
        System.Windows.Forms.DialogResult result = fbd.ShowDialog();

        if (result == System.Windows.Forms.DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
        {
          if (true == System.IO.Path.IsPathRooted(fbd.SelectedPath))
          {
            var folderUri = new Uri(fbd.SelectedPath , UriKind.Absolute);

            string projectFileFolderPath = System.IO.Path.GetDirectoryName(pathToConfigFile);
            var referenceUri = new Uri(projectFileFolderPath, UriKind.Absolute);
            string relativePath = "./" + referenceUri.MakeRelativeUri(folderUri).ToString();

            // Set path in view
            TB_WorkingDataBaseFilePath.Text = relativePath;

            // Set path in project file
            newCfg.WorkingDataBaseFilePath = relativePath;
          }
          else
          {
            MessageBox.Show("Could not convert path. Path is already relative!", "Conversion failure", MessageBoxButton.OK);

            // Set path in view
            TB_WorkingDataBaseFilePath.Text = fbd.SelectedPath;

            // Set path in project file
            newCfg.WorkingDataBaseFilePath = fbd.SelectedPath;

          }
        }
      }
    }

    /// <summary>
    /// Button actions for adding a new input data base file
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void B_AddNewInputDataBaseFile_Click(object sender, RoutedEventArgs e)
    {
      using (var openFileDialog = new System.Windows.Forms.OpenFileDialog())
      {
        openFileDialog.InitialDirectory = absPathModuleBaseFolder;
        openFileDialog.Filter = "Input data base file |*" + ".xml";

        if (System.Windows.Forms.DialogResult.OK == openFileDialog.ShowDialog())
        {

          if (true == System.IO.Path.IsPathRooted(openFileDialog.FileName))
          {
            var folderUri = new Uri(openFileDialog.FileName, UriKind.Absolute);

            string projectFileFolderPath = System.IO.Path.GetDirectoryName(pathToConfigFile);
            var referenceUri = new Uri(projectFileFolderPath + "/", UriKind.Absolute);
            string relativePath = "./" + referenceUri.MakeRelativeUri(folderUri).ToString();

            // Set file path in config
            newCfg.AddNewInputDataBaseFile(relativePath);
          }
          else
          {
            MessageBox.Show("Could not convert path. Path is already relative!", "Conversion failure", MessageBoxButton.OK);

            // Set path in view
            newCfg.AddNewInputDataBaseFile(openFileDialog.FileName);

          }
        }
      }
    }

    /// <summary>
    /// Button actions for removing a selected input data base file
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void B_RemoveSelectedInputDataBaseFile_Click(object sender, RoutedEventArgs e)
    {
      string removedBlock = (string)DG_InputDataBaseFileList.SelectedItem;
      newCfg.RemoveInputDataBaseFile(removedBlock);
    }

    /// <summary>
    /// Interface for working data base target data update
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void DG_WorkingDataBaseContentList_CellEditEnding(object sender, DataGridCellEditEndingEventArgs e)
    {
      newCfg.WorkingDataBaseUpdated();
    }
  }
}
