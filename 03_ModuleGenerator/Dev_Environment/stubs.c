/******************************************************************************
  * @file    stubs.c 
  * @author  Reinemuth Sebastian
  * @date    20-02-2018
  * @brief   stubs-Functions
  * last checkin :
  * $Author: sreinemuth $ @ $Date: 2017-02-18 12:31:21 +0100 (Sa, 18 Feb 2017) $ , Revision: $Rev: 14 $
  *****************************************************************************/

/* Private includes ----------------------------------------------------------*/
#include "stubs.h"
#include "UartIf_Types.h"

/* Global variables ----------------------------------------------------------*/

/* Private function prototypes -----------------------------------------------*/

/* Functions -----------------------------------------------------------------*/
                             //|    ID    |         Data           |         CRC           | EndPattern |
uint8 dummyTestDataForRx[12] = { 0x00, 0x01, 0x01, 0x02, 0x03, 0x04, 0x1B, 0xDA, 0xAC, 0xC2, 0x0D, 0x0A };

/**
* @brief           API function to read a received data frame
* @param[in/out]   UartIf_RxFrameType   Higher layer frame data
* @retval          UartReturnType
*                  >> UARTIF_E_NULL_POINTER
*                  >> UARTIF_E_BUFFER_EMPTY
*                  >> UARTIF_E_BUFFER_OVERFLOW
*                  >> UARTIF_E_OK
*                  >> UARTIF_E_NOT_OK
* @details         This function could be used to read a receive data frame from an uart module
*/
UartIf_ReturnType UartIf_GetReceivedData(UartIf_RxFrameType *dataFrame)
{
  dataFrame->cntOfData = 12;
  dataFrame->pData = &dummyTestDataForRx;
  return UARTIF_E_OK;
}

/**
* @brief           API function to set a transmit data frame
* @param[in/out]   UartIf_TxFrameType   Higher layer frame data
* @retval          UartReturnType
*                  >> UARTIF_E_NULL_POINTER
*                  >> UARTIF_E_BUFFER_EMPTY
*                  >> UARTIF_E_BUFFER_OVERFLOW
*                  >> UARTIF_E_OK
*                  >> UARTIF_E_NOT_OK
* @details         This function could be used to set a new transmit data frame for the uart module
*/
UartIf_ReturnType UartIf_SetTransmitData(UartIf_TxFrameType *dataFrame)
{
  return UARTIF_E_OK;
}