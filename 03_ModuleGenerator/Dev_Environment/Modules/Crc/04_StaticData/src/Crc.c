/*****************************************************************************************************************************
 * @file        Crc.c                                                                                                        *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        07.04.2018 11:39:40                                                                                          *
 * @brief       Implementation of functionalities from the "Crc" module.                                                     *
 *                                                                                                                           *
 * @details     The Crc module implements different Algorithms to perform an cyclic redundancy check.                        *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
 * @libNote     OSAR Source File Library v.0.1.2.15                                                                          *
*****************************************************************************************************************************/
/**
 * @addtogroup Crc
 * @{
 */

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 Start of include area                        << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#include "Det.h"
#include "Crc.h"
#include "Crc32_0x04C11DB7.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                  End of include area                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of private data types                   << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of private data types                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            Start of private definitions area                 << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of private definitions area                  << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of global variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/*------------------------------------------- Map variables into constand memory -------------------------------------------*/
#define Crc_START_SEC_CONST
#include "Crc_MemMap.h"
#define Crc_STOP_SEC_CONST
#include "Crc_MemMap.h"
/*---------------------------------------- Map variables into uninitialized memory ----------------------------------------*/
#define Crc_START_SEC_NOINIT_VAR
#include "Crc_MemMap.h"
#define Crc_STOP_SEC_NOINIT_VAR
#include "Crc_MemMap.h"
/*----------------------------------------- Map variables into initialized memory -----------------------------------------*/
#define Crc_START_SEC_INIT_VAR
#include "Crc_MemMap.h"
#define Crc_STOP_SEC_INIT_VAR
#include "Crc_MemMap.h"
/*--------------------------------------- Map variables into zero initialized memory ---------------------------------------*/
#define Crc_START_SEC_ZERO_INIT_VAR
#include "Crc_MemMap.h"
#define Crc_STOP_SEC_ZERO_INIT_VAR
#include "Crc_MemMap.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of global variables                      << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of private variables                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/*------------------------------------------- Map variables into constand memory -------------------------------------------*/
#define Crc_START_SEC_CONST
#include "Crc_MemMap.h"
#define Crc_STOP_SEC_CONST
#include "Crc_MemMap.h"
/*---------------------------------------- Map variables into uninitialized memory ----------------------------------------*/
#define Crc_START_SEC_NOINIT_VAR
#include "Crc_MemMap.h"
#define Crc_STOP_SEC_NOINIT_VAR
#include "Crc_MemMap.h"
/*----------------------------------------- Map variables into initialized memory -----------------------------------------*/
#define Crc_START_SEC_INIT_VAR
#include "Crc_MemMap.h"
#define Crc_STOP_SEC_INIT_VAR
#include "Crc_MemMap.h"
/*--------------------------------------- Map variables into zero initialized memory ---------------------------------------*/
#define Crc_START_SEC_ZERO_INIT_VAR
#include "Crc_MemMap.h"
#define Crc_STOP_SEC_ZERO_INIT_VAR
#include "Crc_MemMap.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of private variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>           Start of private function prototypes               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            End of private function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>        Start of private function implementation              << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/*--------------------------------------------- Map function into code memory ---------------------------------------------*/
#define Crc_START_SEC_CODE
#include "Crc_MemMap.h"
#define Crc_STOP_SEC_CODE
#include "Crc_MemMap.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>         End of private function implementation               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>          Start of public function implementation             << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/*------------------------------------------------------------  ------------------------------------------------------------*/
#define Crc_START_SEC_CODE
#include "Crc_MemMap.h"
/**
 * @brief           Module global memory initialization function.
 * @param[in]       None
 * @retval          None
 * @details         This function shall be called during system startup. And before calling the init and mainfuntion.
 */
void Crc_InitMemory( void )
{
#if (CRC_USE_SOFT_CALC_CRC32_0X04C11DB7 == STD_ON)
  Crc32_0x04C11DB7_InitMemory();
#endif
}

/**
 * @brief           Module global initialization function.
 * @param[in]       None
 * @retval          None
 * @details         This function shall be called before using the module and after the memory initialization.
 */
void Crc_Init( void )
{
#if (CRC_USE_SOFT_CALC_CRC32_0X04C11DB7 == STD_ON)
  Crc32_0x04C11DB7_Init();
#endif
}

/**
 * @brief           Module global mainfunction.
 * @param[in]       None
 * @retval          None
 * @details         This function shall be called form the actual runtime environment within an fixed cycle.
 */
void Crc_Mainfunction( void )
{
#if (CRC_USE_SOFT_CALC_CRC32_0X04C11DB7 == STD_ON)
#endif
}

#define Crc_STOP_SEC_CODE
#include "Crc_MemMap.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>          End of public function implementation               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/**
 * @}
 */

