﻿/*****************************************************************************************************************************
 * @file        ModuleGenerator.cs                                                                                           *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        06.04.2019                                                                                                   *
 * @brief       Implementation of the Module Generator Class                                                                 *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
* @addtogroup ModuleLibrary.Versions.v_1_0_3.Generator
* @{
*/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OsarResources.Generator;
using OsarResources.Generator.Resources;
using System.Reflection;
using System.Diagnostics;
using System.IO;
using System.Xml.Serialization;
using OsarSourceFileLib.DocumentationObjects.Doxygen;
using OsarSourceFileLib.CFile;
using OsarSourceFileLib.CFile.CFileObjects;
using OsarSourceFileLib.DocumentationObjects.General;
using RteLib;
using ModuleLibrary.Versions.v_1_0_3.Models;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace ModuleLibrary.Versions.v_1_0_3.Generator
{
  internal class ModuleGenerator
  {
    private Models.UartPduRXml xmlCfg;
    private string pathToConfiguratioFile;  // Including config file name
    private string configFilePath;          // Includes only path
    private string pathToModuleBaseFolder;
    private string moduleLibPath;
    private GenInfoType info;
    UartPduRWorkingDataBase uartPduWorkingDataBase;
    List<UartPduInputDatabase> uartPduInputDataBase = new List<UartPduInputDatabase>();

    /* Temporary source code variables */
    string sourceCodeUsedModuleIds = "";
    int sourceCntOfUsedModules = 0;

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="cfgFile"> Config file which shall be generated </param>
    /// <param name="pathToCfgFile"> Path of Configuration file </param>
    /// <param name="absPathToBaseModuleFolder"> Absolute path to module base folder</param>
    public ModuleGenerator(Models.UartPduRXml cfgFile, string pathToCfgFile, string absPathToBaseModuleFolder)
    {
      xmlCfg = cfgFile;
      pathToConfiguratioFile = pathToCfgFile;
      configFilePath = Path.GetDirectoryName(pathToCfgFile);
      pathToModuleBaseFolder = absPathToBaseModuleFolder;

      moduleLibPath = new System.Uri(Assembly.GetExecutingAssembly().CodeBase).AbsolutePath;

      info.info = new List<string>();
      info.log = new List<string>();
      info.warning = new List<string>();
      info.error = new List<string>();
    }

    /// <summary>
    /// Interface to generate the configuration
    /// </summary>
    /// <returns> Generation information </returns>
    public GenInfoType GenerateConfiguration()
    {
      // Merge input data base to working data base
      mergeInputAndWorkingDataBase();
      hlpFnc_SearchInWorkingDataBaseForUsedModules();

      // Generate Memory-Map-Header-File
      GenerateMemoryMapHeaderFile();

      // Generate C-Source-File
      GenerateConfigurationSourceFile();

      // Generate C-Header-File
      GenerateConfigurationHeaderFile();

      // Generate Rte-Interface-XML-File
      GenerateConfigurationRteInterfaceXmlFile();

      return info;
    }

    /// <summary>
    /// Function to merge the input data base with the working data base
    /// </summary>
    public void mergeInputAndWorkingDataBase()
    {
      Boolean pduFound;

      info.AddLogMsg(GenResources.LogMsg_StartMergeOfInputAndWorkingDataBase);

      /* Add data base version */
      

      /* Read working data base */
      if (!File.Exists(configFilePath + xmlCfg.pathOfWorkingDataBase + DefResources.WDB_WorkingDataBaseName))
      {
        uartPduWorkingDataBase = new UartPduRWorkingDataBase();
        uartPduWorkingDataBase.pduConfigList = new List<UartPduR_PduCfg>();
      }
      else
      {
        uartPduWorkingDataBase = ReadWorkingPduDataBaseFromXml();
      }

      /* Add data base version */
      uartPduWorkingDataBase.xmlFileVersion = new OsarResources.XML.XmlFileVersion();
      uartPduWorkingDataBase.xmlFileVersion.MajorVersion = Convert.ToUInt16(DefResources.WDB_FileMajorVersion);
      uartPduWorkingDataBase.xmlFileVersion.MinorVersion = Convert.ToUInt16(DefResources.WDB_FileMinorVersion);
      uartPduWorkingDataBase.xmlFileVersion.PatchVersion = Convert.ToUInt16(DefResources.WDB_FilePatchVersion);

      /* Read input data base */
      foreach (string inputDataBaseFile in xmlCfg.pathOfInputDataBases)
      {
        UartPduInputDatabase inputDataBase = ReadInputPduDataBaseFromXml(configFilePath + inputDataBaseFile);
        uartPduInputDataBase.Add(inputDataBase);
      }


      /* ==================== Start merging files ==================== */
      /* Iterate over all input data bases */
      for (int inputLlistIndex = 0; inputLlistIndex < uartPduInputDataBase.Count(); inputLlistIndex++)
      {
        for (int inputElementIndex = 0; inputElementIndex < uartPduInputDataBase[inputLlistIndex].pduConfigList.Length; inputElementIndex++)
        {
          /* Search if element is available in the working data set. */
          pduFound = false;
          for (int workingElementIndex = 0; workingElementIndex < uartPduWorkingDataBase.pduConfigList.Count(); workingElementIndex++)
          {
            /* Check if PDU Id is identical */
            if (uartPduInputDataBase[inputLlistIndex].pduConfigList[inputElementIndex].pduId == uartPduWorkingDataBase.pduConfigList[workingElementIndex].pduId)
            {
              /* Adapt Data set if needed */
              UartPduR_PduCfg tmpElement = uartPduWorkingDataBase.pduConfigList[workingElementIndex];
              tmpElement.pduShortName = uartPduInputDataBase[inputLlistIndex].pduConfigList[inputElementIndex].pduShortName;
              tmpElement.dataLength = uartPduInputDataBase[inputLlistIndex].pduConfigList[inputElementIndex].dataLength;
              tmpElement.pduDescription = uartPduInputDataBase[inputLlistIndex].pduConfigList[inputElementIndex].pduDescription;
              uartPduWorkingDataBase.pduConfigList[workingElementIndex] = tmpElement;

              pduFound = true;
              break;
            }
          }

          /* Check if no pdu has been found an a new pdu has to be created */
          if (false == pduFound)
          {
            UartPduR_PduCfg tmpPduCfg = new UartPduR_PduCfg();
            tmpPduCfg.moduleID = 255;
            tmpPduCfg.pduId = uartPduInputDataBase[inputLlistIndex].pduConfigList[inputElementIndex].pduId;
            tmpPduCfg.dataLength = uartPduInputDataBase[inputLlistIndex].pduConfigList[inputElementIndex].dataLength;
            tmpPduCfg.pduShortName = uartPduInputDataBase[inputLlistIndex].pduConfigList[inputElementIndex].pduShortName;
            tmpPduCfg.pduDescription = uartPduInputDataBase[inputLlistIndex].pduConfigList[inputElementIndex].pduDescription;
            tmpPduCfg.rxNotificationFnc = GenResources.UartPduRDummyRxNotificationFnc;
            uartPduWorkingDataBase.pduConfigList.Add(tmpPduCfg);
          }
        }
      }

      /* Store merged input data base */
      SaveWorkingPduDataBaseToXml(uartPduWorkingDataBase);

      info.AddLogMsg(GenResources.LogMsg_InputAndWorkingDataBaseMerged);
    }

    /// <summary>
    /// Interface to generate the Memory-Map-Header-File
    /// </summary>
    /// <returns> Generation information </returns>
    private void GenerateMemoryMapHeaderFile()
    {
      info.AddLogMsg(GenResources.LogMsg_StartGenMemMap);

      /* +++++ Generate Memory Mapping file +++++ */
      OsarResources.Generic.OsarGenericHelper.generateModuleMemoryMappingFile(pathToModuleBaseFolder + GenResources.GenHeaderFilePath, DefResources.ModuleName, DefResources.GeneratorName);

      info.AddLogMsg(GenResources.LogMsg_MemMapGenerated);
    }

    /// <summary>
    /// Interface to generate the configuration C-Source-File
    /// </summary>
    /// <returns> Generation information </returns>
    private void GenerateConfigurationSourceFile()
    {
      info.AddLogMsg(GenResources.LogMsg_GenSourceCfgFile);

      CSourceFile modulePBCfgSource = new CSourceFile();
      DoxygenFileHeader doxygenFileHeader = new DoxygenFileHeader();
      DoxygenFileGroupOpener doxygenFileGroupOpener = new DoxygenFileGroupOpener();
      DoxygenFileGroupCloser doxygenFileGroupCloser = new DoxygenFileGroupCloser();
      GeneralStartGroupObject generalStartGroup = new GeneralStartGroupObject();
      GeneralEndGroupObject generalEndGroup = new GeneralEndGroupObject();
      CFileIncludeObjects sourceIncludes = new CFileIncludeObjects();
      CFileVariableObjects sourceVariables = new CFileVariableObjects();
      CFileFunctionObjects sourceFunction = new CFileFunctionObjects();
      DoxygenElementDescription doxygenDescription = new DoxygenElementDescription();
      int actualGroupIdx;
      string tempString = "", tempString2 = "";

      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* ++++++++++++++++++++++++++++++++++++++++ Prepare general data sets ++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      doxygenFileHeader.AddFileBrief("Implementation of UartPduR module *_PBCfg.c file");
      doxygenFileHeader.AddFileName(DefResources.ModuleName + "_PBCfg.c");
      doxygenFileHeader.AddFileNote(OsarResources.Generator.Resources.genericSource.CommentGeneratorDisclaimer);
      doxygenFileHeader.AddFileAuthor(DefResources.GenFileAuthor);
      doxygenFileHeader.AddFileGenerator(DefResources.GeneratorName + " v." + ModuleLibraryVersionClass.getCurrentVersion());
      doxygenFileHeader.AddFileVersion(DefResources.ModuleVersion);
      doxygenFileGroupOpener.AddToDoxygenMasterGroup(OsarResources.Generator.Resources.genericSource.DoxygenOsarMasterGroupBSW);
      doxygenFileGroupOpener.AddToDoxygenGroup(DefResources.ModuleName);

      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* ++++++++++++++++++++++++++++++++++++++++++ Prepare include strings ++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      sourceIncludes.AddIncludeFile(DefResources.ModuleName + "_PBCfg.h");

      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* ++++++++++++++++++++++++++++++++++++++++++++ Prepare Variables ++++++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* >> Add constant variable */
      generalStartGroup.AddGroupName("Map variables into constant memory");
      generalStartGroup.AddOsarStartMemMapDefine(DefResources.ModuleName, OsarMemMapDataType.OSAR_CONST);
      generalEndGroup.AddOsarStopMemMapDefine(DefResources.ModuleName, OsarMemMapDataType.OSAR_CONST);
      actualGroupIdx = sourceVariables.AddCFileObjectGroup(generalStartGroup, generalEndGroup);

      /* Pdu End Pattern */
      tempString = "";
      for (int subIdx = 0; subIdx < xmlCfg.uartFrameEndPatternElements.Length; subIdx++)
      {
        if (subIdx != 0)
        { tempString += ", "; }
        tempString += xmlCfg.uartFrameEndPatternElements[subIdx].ToString();
      }
      sourceVariables.AddArray("uint8", "pduEndPattern", xmlCfg.uartFrameEndPatternElements.Length.ToString(),
        actualGroupIdx, tempString, "const");

      /* List with used Module Ids */
      sourceVariables.AddArray("uint8", "listOfUsedUartModules", GenResources.UartPduRDefineCountOfUsedModules, 
        actualGroupIdx, sourceCodeUsedModuleIds, "const");

      /* List with configured PDUs */
      tempString = "";
      for (int subIdx = 0; subIdx < uartPduWorkingDataBase.pduConfigList.Count(); subIdx++)
      {
        if (subIdx == 0)
        {
          tempString += "{";
        }
        else
        {
          tempString += ", {";
        }
        tempString += uartPduWorkingDataBase.pduConfigList[subIdx].moduleID.ToString() + ", ";
        tempString += uartPduWorkingDataBase.pduConfigList[subIdx].pduId.ToString() + ", ";
        tempString += uartPduWorkingDataBase.pduConfigList[subIdx].dataLength.ToString() + ", ";

        if (uartPduWorkingDataBase.pduConfigList[subIdx].rxNotificationFnc != GenResources.UartPduRDummyRxNotificationFnc)
        {
          tempString += "&";
        }
        tempString += uartPduWorkingDataBase.pduConfigList[subIdx].rxNotificationFnc + "}";
      }
      sourceVariables.AddArray("UartPduR_PduConfigType", "uartPduR_PduConfigList", 
        GenResources.UartPduRDefineCountOfConfiguredPdus, actualGroupIdx, tempString, "const");

      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++ Prepare Source Functions ++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* ++++++++++++++++++++++++++++++++++++++++ Build up and generate file +++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      modulePBCfgSource.AddFileName(DefResources.ModuleName + "_PBCfg.c");
      modulePBCfgSource.AddFilePath(pathToModuleBaseFolder + GenResources.GenSourceFilePath);
      modulePBCfgSource.AddFileCommentHeader(doxygenFileHeader);
      modulePBCfgSource.AddFileGroup(doxygenFileGroupOpener, doxygenFileGroupCloser);
      modulePBCfgSource.AddIncludeObject(sourceIncludes);
      modulePBCfgSource.AddGlobalVariableObject(sourceVariables);
      modulePBCfgSource.AddGlobalFunctionObject(sourceFunction);
      modulePBCfgSource.GenerateSourceFile();

      info.AddLogMsg(GenResources.LogMsg_SourceCfgFileGenerated);
    }

    /// <summary>
    /// Interface to generate the configuration C-Header-File
    /// </summary>
    /// <returns> Generation information </returns>
    private void GenerateConfigurationHeaderFile()
    {
      info.AddLogMsg(GenResources.LogMsg_GenHeaderCfgFile);

      CHeaderFile modulePBCfgHeader = new CHeaderFile();
      DoxygenFileHeader doxygenFileHeader = new DoxygenFileHeader();
      DoxygenFileGroupOpener doxygenFileGroupOpener = new DoxygenFileGroupOpener();
      DoxygenFileGroupCloser doxygenFileGroupCloser = new DoxygenFileGroupCloser();
      CFileDefinitionObjects headerDefinitions = new CFileDefinitionObjects();
      GeneralStartGroupObject generalStartGroup = new GeneralStartGroupObject();
      GeneralEndGroupObject generalEndGroup = new GeneralEndGroupObject();
      CFileIncludeObjects headerIncludes = new CFileIncludeObjects();
      CFileVariableObjects headerVariables = new CFileVariableObjects();
      DoxygenElementDescription doxygenDescription = new DoxygenElementDescription();
      CFileFunctionObjects headerFunctions = new CFileFunctionObjects();
      CFileTypeObjects headerTypes = new CFileTypeObjects();
      int actualGroupIdx;
      string tempString = "", tempString2 = "";

      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* ++++++++++++++++++++++++++++++++++++++++ Prepare general data sets ++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      doxygenFileHeader.AddFileBrief("Generated header file data of the UartPduR module.");
      doxygenFileHeader.AddFileName(DefResources.ModuleName + "_PBCfg.h");
      doxygenFileHeader.AddFileNote(OsarResources.Generator.Resources.genericSource.CommentGeneratorDisclaimer);
      doxygenFileHeader.AddFileAuthor(DefResources.GenFileAuthor);
      doxygenFileHeader.AddFileGenerator(DefResources.GeneratorName + " v." + ModuleLibraryVersionClass.getCurrentVersion());
      doxygenFileHeader.AddFileVersion(DefResources.ModuleVersion);
      doxygenFileGroupOpener.AddToDoxygenMasterGroup(OsarResources.Generator.Resources.genericSource.DoxygenOsarMasterGroupBSW);
      doxygenFileGroupOpener.AddToDoxygenGroup(DefResources.ModuleName);

      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* ++++++++++++++++++++++++++++++++++++++++++ Prepare include strings ++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      headerIncludes.AddIncludeFile(DefResources.ModuleName + ".h");


      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* ++++++++++++++++++++++++++++++++++++++++++++ Prepare data types +++++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++ Prepare Definitions +++++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* Create Det informations */
      generalStartGroup.AddGroupName("UartPduR Module Det Information");
      actualGroupIdx = headerDefinitions.AddCFileObjectGroup(generalStartGroup, generalEndGroup);

      headerDefinitions.AddDefinition(GenResources.GenDefineDetModuleId,
        xmlCfg.detModuleID.ToString(), actualGroupIdx);

      headerDefinitions.AddDefinition(GenResources.GenDefineModuleUseDet,
        xmlCfg.detModuleUsage.ToString(), actualGroupIdx);

      /* Create General module configuration parameters */
      generalStartGroup.AddGroupName("General module configuration parameters");
      actualGroupIdx = headerDefinitions.AddCFileObjectGroup(generalStartGroup, generalEndGroup);

      headerDefinitions.AddDefinition(GenResources.UartPduRDefineCountOfFrameEndPattern, 
        xmlCfg.uartFrameEndPatternElements.Length.ToString(), actualGroupIdx);

      headerDefinitions.AddDefinition(GenResources.UartPduRDefineCountOfUsedModules,
        sourceCntOfUsedModules.ToString(), actualGroupIdx);

      headerDefinitions.AddDefinition(GenResources.UartPduRDefineCountOfConfiguredPdus, 
        uartPduWorkingDataBase.pduConfigList.Count().ToString(), actualGroupIdx);

      headerDefinitions.AddDefinition(GenResources.UartPduRDefineMaxUsedDataBytes, 
        xmlCfg.uartIfMaxFrameDataSize.ToString(), actualGroupIdx);

      headerDefinitions.AddDefinition(GenResources.UartPduRDefineUseCRC32EthernetE2ECommunication, 
        xmlCfg.useCrc32EthernetE2ECommunication.ToString(), actualGroupIdx);

      /* Create Pdu specific transmission functions */
      generalStartGroup.AddGroupName("Pdu specific transmission functions");
      actualGroupIdx = headerDefinitions.AddCFileObjectGroup(generalStartGroup, generalEndGroup);
      tempString = "";
      tempString2 = "";
      for (int idx = 0; idx < uartPduWorkingDataBase.pduConfigList.Count(); idx++)
      {
        tempString = "UartPduR_SendUartPdu_" + uartPduWorkingDataBase.pduConfigList[idx].pduId.ToString() +
          "_" + uartPduWorkingDataBase.pduConfigList[idx].pduShortName + "(pData)";

        tempString2 = "UartPduR_SendUartPdu(" + uartPduWorkingDataBase.pduConfigList[idx].pduId.ToString() +
          ", pData, " + uartPduWorkingDataBase.pduConfigList[idx].dataLength.ToString() + ")";

        headerDefinitions.AddDefinition(tempString, tempString2, actualGroupIdx);
      }

      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++ Prepare Header Variables ++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* Create General Buffer References */
      generalStartGroup.AddGroupName("Uart PduR configuration buffers");
      actualGroupIdx = headerVariables.AddCFileObjectGroup(generalStartGroup, generalEndGroup);

      headerVariables.AddArray("uint8", "pduEndPattern", "", actualGroupIdx, "", "extern const");
      headerVariables.AddArray("uint8", "listOfUsedUartModules", "", actualGroupIdx, "", "extern const");
      headerVariables.AddArray("UartPduR_PduConfigType", "uartPduR_PduConfigList", "", 
        actualGroupIdx, "", "extern const");

      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++ Prepare Header Functions ++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

      /* Provide generated function prototypes */
      generalStartGroup.AddGroupName("Provide extern function notification prototypes");
      actualGroupIdx = headerFunctions.AddCFileObjectGroup(generalStartGroup, generalEndGroup);

      for (int idx = 0; idx < uartPduWorkingDataBase.pduConfigList.Count(); idx++)
      {
        if (uartPduWorkingDataBase.pduConfigList[idx].rxNotificationFnc != GenResources.UartPduRDummyRxNotificationFnc)
        {
          headerFunctions.AddFunction("extern void", uartPduWorkingDataBase.pduConfigList[idx].rxNotificationFnc,
            "uint8 *pData, uint16 cntData", actualGroupIdx);
        }
      }

      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* ++++++++++++++++++++++++++++++++++++++++ Build up and generate file +++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      modulePBCfgHeader.AddFileName(DefResources.ModuleName + "_PBCfg.h");
      modulePBCfgHeader.AddFilePath(pathToModuleBaseFolder + GenResources.GenHeaderFilePath);
      modulePBCfgHeader.AddFileCommentHeader(doxygenFileHeader);
      modulePBCfgHeader.AddFileGroup(doxygenFileGroupOpener, doxygenFileGroupCloser);
      modulePBCfgHeader.AddDefinitionObject(headerDefinitions);
      modulePBCfgHeader.AddIncludeObject(headerIncludes);
      modulePBCfgHeader.AddGlobalVariableObject(headerVariables);
      modulePBCfgHeader.AddGlobalFunctionPrototypeObject(headerFunctions);
      modulePBCfgHeader.AddTypesObject(headerTypes);
      modulePBCfgHeader.GenerateSourceFile();

      info.AddLogMsg(GenResources.LogMsg_HeaderCfgFileGenerated);
    }

    /// <summary>
    /// Interface to generate the configuration Rte-Interface-XML-File
    /// </summary>
    /// <returns> Generation information </returns>
    private void GenerateConfigurationRteInterfaceXmlFile()
    {
      info.AddLogMsg(GenResources.LogMsg_GenRteMibCfgFile);
      info.AddLogMsg("Using RteLib version: v." + RteLib.VersionClass.major.ToString() + "." +
        RteLib.VersionClass.minor.ToString() + "." + RteLib.VersionClass.patch.ToString());

      // Create main instance of internal behavior file
      RteLib.RteModuleInternalBehavior.RteModuleInternalBehavior internalBehavior = new RteLib.RteModuleInternalBehavior.RteModuleInternalBehavior();
      RteLib.RteModuleInternalBehavior.RteRunnable initRunnable = new RteLib.RteModuleInternalBehavior.RteRunnable();
      RteLib.RteModuleInternalBehavior.RteCyclicRunnable mainfunctionRunnable = new RteLib.RteModuleInternalBehavior.RteCyclicRunnable();

      // Create Port information
      RteLib.RteModuleInternalBehavior.RteAvailableServerPort csI_PpGeneralUartPduR = new RteLib.RteModuleInternalBehavior.RteAvailableServerPort();

      // Create Interface Blueprint Instances
      // gii == General Interface Info
      // ibp == Interface Blue Print
      // cs == Client Server
      RteLib.RteInterface.RteCSInterfaceBlueprint cs_ibp_PpGeneralUartPduR = new RteLib.RteInterface.RteCSInterfaceBlueprint();
      RteLib.RteInterface.RteGeneralInterfaceInfo gii_ibp_PpGeneralUartPduR = new RteLib.RteInterface.RteGeneralInterfaceInfo();

      // Helper Objects
      RteLib.RteInterface.RteGeneralInterfaceInfo generalInterfaceInfo;
      RteLib.RteInterface.RteGeneralInterfaceInfo runabbleInfo = new RteLib.RteInterface.RteGeneralInterfaceInfo();
      RteLib.RteModuleInternalBehavior.RteRunnable rteRunnable;
      RteLib.RteInterface.RteFunctionBlueprint rteFncBlueprint;
      RteLib.RteInterface.RteFunctionArgumentBlueprint rteFncArgBlueprint;

      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++ Create Types ++++++++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      RteLib.RteTypes.RteBaseTypesResource rteBaseTypesResource = new RteLib.RteTypes.RteBaseTypesResource();

      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* ++++++++++++++++++++++++++++++++++++++++ Create Blueprint Interfaces CS +++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* ########## Adding Server Port Interface Blueprint "PiGeneralUartPduR" ########## */
      #region Adding Server Port Interface Blueprint "PiGeneralUartPduR"
      // Create general prototype information
      gii_ibp_PpGeneralUartPduR.InterfaceName = "PiGeneralUartPduR";
      gii_ibp_PpGeneralUartPduR.UUID = "2dd0a67f35ca4b95858b533ac283bca8";
      cs_ibp_PpGeneralUartPduR.InterfaceInfo = gii_ibp_PpGeneralUartPduR; //Adding information to PiGeneralUartPduR Interface Blueprint
      cs_ibp_PpGeneralUartPduR.Functions = new List<RteLib.RteInterface.RteFunctionBlueprint>();

      #region Function Nvm_GetErrorStatus
      rteFncBlueprint = new RteLib.RteInterface.RteFunctionBlueprint();
      rteFncBlueprint.ReturnValue = rteBaseTypesResource.Rte_ErrorType_Type.DataType;
      rteFncBlueprint.AvailableErrorTypes = new List<string>();
      rteFncBlueprint.AvailableErrorTypes.Add("NullPointer");
      rteFncBlueprint.AvailableErrorTypes.Add("InvalidPduLength");
      rteFncBlueprint.AvailableErrorTypes.Add("UnknownId");
      rteFncBlueprint.AvailableErrorTypes.Add("TransmitBufferOverflow");
      rteFncBlueprint.AvailableErrorTypes.Add("GenericProgrammingFailure");
      rteFncBlueprint.FunctionName = "UartPduR_SendUartPdu";
      rteFncBlueprint.UUID = "fe6d9cdddb9142329221c4b5812bff8d";

      rteFncBlueprint.ArgList = new List<RteLib.RteInterface.RteFunctionArgumentBlueprint>();
      rteFncArgBlueprint = new RteLib.RteInterface.RteFunctionArgumentBlueprint();
      rteFncArgBlueprint.ArgumentName = "pduId";
      rteFncArgBlueprint.ArgumentType = rteBaseTypesResource.Uint16_Type;
      rteFncArgBlueprint.ArgumentType.DataAccessType = RteLib.RteTypes.RteDataAccessType.STANDARD;
      rteFncBlueprint.ArgList.Add(rteFncArgBlueprint);

      rteFncArgBlueprint = new RteLib.RteInterface.RteFunctionArgumentBlueprint();
      rteFncArgBlueprint.ArgumentName = "pData";
      rteFncArgBlueprint.ArgumentType = rteBaseTypesResource.Uint8_Type;
      rteFncArgBlueprint.ArgumentType.DataAccessType = RteLib.RteTypes.RteDataAccessType.POINTER;
      rteFncBlueprint.ArgList.Add(rteFncArgBlueprint);

      rteFncArgBlueprint = new RteLib.RteInterface.RteFunctionArgumentBlueprint();
      rteFncArgBlueprint.ArgumentName = "dataLength";
      rteFncArgBlueprint.ArgumentType = rteBaseTypesResource.Uint16_Type;
      rteFncArgBlueprint.ArgumentType.DataAccessType = RteLib.RteTypes.RteDataAccessType.STANDARD;
      rteFncBlueprint.ArgList.Add(rteFncArgBlueprint);

      cs_ibp_PpGeneralUartPduR.Functions.Add(rteFncBlueprint);
      #endregion

      #endregion

      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* ++++++++++++++++++++++++++++++++++++++++ Create Blueprint Interfaces SR +++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* ++++++++++++++++++++++++++++++++++++++++++ Create CS Port Interfaces ++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* ########## Adding Server Port "PpGeneralUartPduR" ########## */
      #region Adding Server Port "PpGeneralUartPduR"
      generalInterfaceInfo = new RteLib.RteInterface.RteGeneralInterfaceInfo();
      generalInterfaceInfo.InterfaceName = "PpGeneralUartPduR";
      generalInterfaceInfo.UUID = "43b6ef0333434283b20a5da4ba64f392";

      RteLib.RteInterface.RteCSInterfacePrototype csIpt_PpGeneralUartPduR = new RteLib.RteInterface.RteCSInterfacePrototype();
      csIpt_PpGeneralUartPduR.InterfacePrototype = generalInterfaceInfo;
      csIpt_PpGeneralUartPduR.InterfaceBlueprint = gii_ibp_PpGeneralUartPduR;
      csIpt_PpGeneralUartPduR.InterfaceType = RteLib.RteInterface.RteCSInterfaceImplementationType.SERVER;
      csI_PpGeneralUartPduR.ServerPort = csIpt_PpGeneralUartPduR; //Adding information to PpGeneralUartPduR Port

      // Create Runnable Information
      csI_PpGeneralUartPduR.ServerPortRunnables = new List<RteLib.RteModuleInternalBehavior.RteRunnable>();
      rteRunnable = new RteLib.RteModuleInternalBehavior.RteRunnable();
      runabbleInfo = new RteLib.RteInterface.RteGeneralInterfaceInfo();
      runabbleInfo.InterfaceName = "UartPduR_SendUartPdu";
      runabbleInfo.UUID = "fe6d9cdddb9142329221c4b5812bff8d";
      rteRunnable.RunnableInfo = runabbleInfo;
      csI_PpGeneralUartPduR.ServerPortRunnables.Add(rteRunnable); //Adding information to PpGeneralUartPduR Port

      #endregion
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* ++++++++++++++++++++++++++++++++++++++++++ Create SR Port Interfaces ++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++ Create Init Runnable ++++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      runabbleInfo = new RteLib.RteInterface.RteGeneralInterfaceInfo();
      runabbleInfo.InterfaceName = GenResources.RteMib_InitRunnableName;
      runabbleInfo.UUID = GenResources.RteMib_InitRunnableName_UUID;

      initRunnable.RunnableInfo = runabbleInfo;

      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* ++++++++++++++++++++++++++++++++++++++++++ Create Cyclic Runnable +++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      runabbleInfo = new RteLib.RteInterface.RteGeneralInterfaceInfo();
      runabbleInfo.InterfaceName = GenResources.RteMib_MainfunctionRunnableName;
      runabbleInfo.UUID = GenResources.RteMib_MainfunctionRunnableName_UUID;

      rteRunnable = new RteLib.RteModuleInternalBehavior.RteRunnable();
      rteRunnable.RunnableInfo = runabbleInfo;

      mainfunctionRunnable = new RteLib.RteModuleInternalBehavior.RteCyclicRunnable();
      mainfunctionRunnable.Runnable = rteRunnable;
      mainfunctionRunnable.CycleTime = xmlCfg.miBMainfunctionCycleTimeMs;

      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* ++++++++++++++++++++++++++++++++++++++++ Build up and generate file +++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      internalBehavior.ModuleName = DefResources.ModuleName;
      internalBehavior.ModuleType = RteLib.RteConfig.RteConfigModuleTypes.BSW;
      internalBehavior.UUID = GenResources.RteMib_Module_UUID;
      internalBehavior.InitRunnables.Add(initRunnable);
      internalBehavior.CyclicRunnables.Add(mainfunctionRunnable);
      internalBehavior.AvailableServerPorts.Add(csI_PpGeneralUartPduR);
      internalBehavior.BackupCSBlueprintList.Add(cs_ibp_PpGeneralUartPduR);

      internalBehavior.SaveActiveRteModuleInternalBehaviorToXml(pathToModuleBaseFolder + GenResources.GenGeneratorFilePath + "\\" + GenResources.RteCfgFileName);

      info.AddLogMsg(GenResources.LogMsg_RteMibCfgFileGenerated);
    }

    

    /// <summary>
    /// Helper function to evaluate the count of used modules and their ids
    /// </summary>
    private void hlpFnc_SearchInWorkingDataBaseForUsedModules()
    {
      List<Byte> moduleIds = new List<Byte>();

      /* Check each data element of the working data base */
      for (int idx = 0; idx < uartPduWorkingDataBase.pduConfigList.Count(); idx++)
      {
        if (false == moduleIds.Contains(uartPduWorkingDataBase.pduConfigList[idx].moduleID))
        {
          moduleIds.Add(uartPduWorkingDataBase.pduConfigList[idx].moduleID);
          if (moduleIds.Count == 1)
          {
            sourceCodeUsedModuleIds += uartPduWorkingDataBase.pduConfigList[idx].moduleID.ToString();
          }
          else
          {
            sourceCodeUsedModuleIds += "," + uartPduWorkingDataBase.pduConfigList[idx].moduleID.ToString();
          }
        }
      }
      sourceCntOfUsedModules = moduleIds.Count();
    }

    /// <summary>
    /// Function to read the current configuration structure from an xml file
    /// </summary>
    /// <returns></returns>
    private UartPduRWorkingDataBase ReadWorkingPduDataBaseFromXml()
    {
      UartPduRWorkingDataBase uartPduWorkingDataBase = new UartPduRWorkingDataBase();

      XmlSerializer reader = new XmlSerializer(uartPduWorkingDataBase.GetType());
      StreamReader file = new StreamReader(configFilePath + xmlCfg.pathOfWorkingDataBase + DefResources.WDB_WorkingDataBaseName);
      uartPduWorkingDataBase = (UartPduRWorkingDataBase)reader.Deserialize(file);
      file.Close();
      return uartPduWorkingDataBase;
    }

    /// <summary>
    /// Function to store the current configuration structure to an xml file
    /// </summary>
    /// <param name="uartPduWorkingDataBase"></param>
    private void SaveWorkingPduDataBaseToXml(UartPduRWorkingDataBase uartPduWorkingDataBase)
    {
      XmlSerializer writer = new XmlSerializer(uartPduWorkingDataBase.GetType());
      StreamWriter file = new StreamWriter(configFilePath + xmlCfg.pathOfWorkingDataBase + DefResources.WDB_WorkingDataBaseName);
      writer.Serialize(file, uartPduWorkingDataBase);
      file.Close();
    }

    /// <summary>
    /// Read input data base
    /// </summary>
    /// <param name="pathAndNameOfDataBase"></param>
    /// <returns></returns>
    private UartPduInputDatabase ReadInputPduDataBaseFromXml(string pathAndNameOfDataBase)
    {
      UartPduInputDatabase tempInputDataBase = new UartPduInputDatabase();
      XmlSerializer reader = new XmlSerializer(tempInputDataBase.GetType());
      StreamReader file = new StreamReader(pathAndNameOfDataBase);
      tempInputDataBase = (UartPduInputDatabase)reader.Deserialize(file);
      file.Close();
      return tempInputDataBase;
    }


  }
}
