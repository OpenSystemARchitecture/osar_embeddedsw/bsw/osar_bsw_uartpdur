/*****************************************************************************************************************************
 * @file        UartIf_Types.h                                                                                               *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        25.02.2018 07:31:55                                                                                          *
 * @brief       Implementation of module global datatypes from the "UartIf" module.                                          *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
 * @libNote     OSAR Source File Library v.0.1.2.15                                                                          *
*****************************************************************************************************************************/
#ifndef __UARTIF_TYPES_H
#define __UARTIF_TYPES_H
/**
 * @addtogroup UartIf
 * @{
 */

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 Start of include area                        << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#include "Std_Types.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                  End of include area                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of public data types                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/* ######################################################################################################################## */
/* ############################################### Uart Module base types ################################################# */
/* ######################################################################################################################## */

/**
 * @brief           Available application errors of the Uart
 * @details         This enumeration implements all available application return and error values within this specific 
 *                  module. The Application error coudl be used as DET information or as return type of internal functions. 
 */
typedef enum {
  UARTIF_E_OK = 0,                                      /*!<  Function call successful */
  UARTIF_E_NOT_OK = 1,                                  /*!<  Function call failed */
  UARTIF_E_PENDING = 2,                                 /*!<  Function call pending */
  UARTIF_E_NOT_IMPLEMENTED = 3,                         /*!<  Function logic not implemented */
  UARTIF_E_BUFFER_FULL = 4,                             /*!<  Requested buffer is full */
  UARTIF_E_BUFFER_EMPTY = 5,                            /*!<  Requested buffer is empty */
  UARTIF_E_BUFFER_OVERFLOW = 6,                         /*!<  Error >> An buffer overflow has bee detected */
  UARTIF_E_INVALID_PARAMETER = 7,                       /*!<  Error >> An invalid parameter has been detected */
  UARTIF_E_INDEX_OUT_OF_RANGE = 100,                    /*!<  Error >> Requested index is out of range */
  UARTIF_E_NULL_POINTER = 101,                          /*!<  Error >> Null pointer detected */
  UARTIF_E_GENERIC_PROGRAMMING_FAILURE = 102            /*!<  Error >> Generic / unspecified error detected */
}UartIf_ReturnType;

/**
 * @brief           Available application return values of the Uart
 * @details         Redefinition of the UartIf_ReturnType as error type.
 */
typedef UartIf_ReturnType UartIf_ErrorType;

/**
* @brief           Uart state machine states
*/
typedef enum {
  UART_STATEMACHINESTATE_UNINIT,
  UART_STATEMACHINESTATE_RUN,
  UART_STATEMACHINESTATE_SAFE
}UartIf_StateMachineStateType;



/**
* @brief           Uart hardware configuration type
*/
typedef struct {
  void(*rxNotificationFnc)(uint8);              /*!< Function pointer to the higher layer module notification function if rx data has bee received */
  void(*rxIsrNotificationFnc)(uint8);           /*!< Function pointer to the higher layer module notification function if rx interrupt data has bee received */
  void(*txNotificationFnc)(uint8);              /*!< Function pointer to the higher layer module notification function if rx data has bee received */
  void(*txIsrNotificationFnc)(uint8);           /*!< Function pointer to the higher layer module notification function if tx interrupt data has bee received */
}UartIf_ModuleConfigType;


/* ######################################################################################################################## */
/* ################################################ Uart buffer data types ################################################ */
/* ######################################################################################################################## */
/**
* @brief           Available frame status type values
* @details         This enumeration implements all available frame status types. It is used to control the transmission of an UART Frame.
*/
typedef enum {
  UART_FRAME_READY = 0,                       /*!< Actual frame is ready to be processed */
  UART_FRAME_OK,                              /*!< Actual frame processed ends successful */
  UART_FRAME_PENDING,                         /*!< Actual frame processing is in progress */
  UART_FRAME_ERROR,                           /*!< Actual frame processing failure detected >> Processing abored */
  UART_FRAME_INVALID                          /*!< Actual frame data is not valid >> Initial state of an frame */
}UartIf_FrameStatus;

/**
 * @brief          Basic configuration data of an uart buffer
 */
typedef struct {
  uint8 cntOfBufferFrames;                    /*!< Count of frames within this buffer */
  uint8 readElementPos;                       /*!< Current read element possition */
  uint8 writeElementPos;                      /*!< Current write element possition */
  uint8 sizeOfBufferFrameData;                /*!< Size of the data within the frame structure */
  uint16 framBufferStartIdx;                  /*!< Start index of the frame buffer */
}UartIf_Buffer_Ctrl;

/**
* @brief          Basic buffer frame data structure
*/
typedef struct {
  uint8 frameDataLength;                      /*!< Length of frame data which shall be used for transmission */
  uint8* pToActualFrameData;                  /*!< Pointer to actual frame data */
  UartIf_FrameStatus frameStatus;             /*!< Actual Frame status */
}UartIf_Buffer_Frame;

/* ######################################################################################################################## */
/* ############################################## Uart interface data types ############################################### */
/* ######################################################################################################################## */
/**
* @brief           Uart tx frame data
*/
typedef struct {
  uint8* pData;                               /*!< Pointer where tx / rx frame data is / shoudl be stored */
  uint8 cntOfData;                            /*!< Cnt of data elements in bytes */
  uint8 moduleID;                             /*!< Module id which shall use this frame */
}UartIf_TxFrameType;

/**
* @brief           Uart tx frame data
*/
typedef struct {
  uint8* pData;                               /*!< Pointer where tx / rx frame data is / shoudl be stored */
  uint8 cntOfData;                            /*!< Cnt of data elements in bytes */
  uint8 moduleID;                             /*!< Module id which shall use this frame */
  UartIf_FrameStatus frameState;                  /*!< Frame status */
}UartIf_RxFrameType;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of public data types                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            Start of public definition area                   << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of public definition area                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of global variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of global variables                      << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>           Start of public function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of public function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/**
 * @}
 */

#endif /* __UARTIF_TYPES_H*/
