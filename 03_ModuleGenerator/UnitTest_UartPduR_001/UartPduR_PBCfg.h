/*****************************************************************************************************************************
 * @file        UartPduR_PBCfg.h                                                                                             *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        25.03.2018 12:37:53                                                                                          *
 * @brief       Generated header file data of the UartPduR module.                                                           *
 * @details     Definition of general generated data.                                                                        *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
 * @libNote     OSAR Source File Library v.0.1.2.15                                                                          *
*****************************************************************************************************************************/
#ifndef __UARTPDUR_PBCFG_H
#define __UARTPDUR_PBCFG_H
/**
 * @addtogroup UartPduR
 * @{
 */

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 Start of include area                        << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#include "Std_Types.h"
#include "UartPduR_Types.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                  End of include area                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of public data types                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of public data types                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            Start of public definition area                   << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/*----------------------------------  Development error trace information for this module ----------------------------------*/
#define UARTPDUR_DET_MODULE_ID  3
#define UARTPDUR_MODULE_USE_DET STD_ON
/*----------------------------------------------- General PDU Informations -------------------------------------------------*/
#define UARTPDUR_CNT_OF_CONFIGURED_PDUS 4
#define UARTPDUR_CNT_PDU_END_PATTERN 2
#define UARTPDUR_CNT_OF_GLOBAL_MAX_DATA_BYTES 20
#define UARTPDUR_CNT_OF_USED_UART_MODULES 1
#define UARTPDUR_USE_CRC32ETHERNET_E2E_COMMUNICATION STD_OFF
/*--------------------------------------------- Generated Interface functions ----------------------------------------------*/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of public definition area                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of global variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
extern const UartPduR_PduConfigType uartPduR_PduConfigList[UARTPDUR_CNT_OF_CONFIGURED_PDUS];
extern const uint8 pduEndPattern[UARTPDUR_CNT_PDU_END_PATTERN];
extern const uint8 listOfUsedUartModules[];
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of global variables                      << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>           Start of public function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
extern void testReceiveFncPduId0(uint8* data, uint16 length);
extern void testReceiveFncPduId1(uint8* data, uint16 length);
extern void testReceiveFncPduId4(uint8* data, uint16 length);
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of public function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/**
 * @}
 */

#endif /* __UARTPDUR_PBCFG_H*/
