﻿/*****************************************************************************************************************************
 * @file        UartPduRWorkingDataBase.cs                                                                                          *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        28.12.2019                                                                                                   *
 * @brief       Implementation of the Working Data Base Data Model                                                           *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
 * @addtogroup ModuleLibrary.Versions.v_1_0_3.Models
 * @{
 */

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using OsarResources.XML;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace ModuleLibrary.Versions.v_1_0_3.Models
{
  public struct UartPduR_PduCfg
  {
    public Byte moduleID;                       /*!< Module Id where the Pdu is used */
    public UInt16 pduId;                        /*!< ID of the PDU */
    public UInt16 dataLength;                   /*!< Length of used data within the PDU >> Without frame end pattern */
    public String rxNotificationFnc;            /*!< Function pointer to the higher layer module notification function if rx data has bee received */
    public String pduShortName;                 /*!< Sort name of the PDU */
    public String pduDescription;               /*!< Unused Pdu Description */
  }

  public class UartPduRWorkingDataBase
  {
    public XmlFileVersion xmlFileVersion;
    public List<UartPduR_PduCfg> pduConfigList;
  }
}
